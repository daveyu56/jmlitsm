﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atos.Venom.Core.Process;
using System.IO;
using System.Net.Mail;
using JML.ITSM.Logging;
using JML.ITSM.BusinessLogic;
using JML.ITSM.DAL;
using JML.ITSM.Model;
using Ninject;
using JML.ITSM.WebServices.JMLITSMReference;
using System.Threading;


namespace JML.ITSM.Service.ITSMCompleteTicket
{
    public class ITSMCompleteTicketProcess : Process
    {


        [Inject]
        public IITSMRepository Repository { get; set; }
       
        [Inject]
        public JMLITSMEntities Context {get; set;}
              
        [Inject]
        public ITSMDirector Director {get; set;}

        [Inject]
        public SettingRepository Settings { get; set; }

        private readonly string Wait_Complete_Ms = "Wait_Complete_Ms";

        private const int WAIT_COMPLETE_MS_DEFAULT = 120000;

        public ITSMCompleteTicketProcess() 
            : base()
        {
            var kernel = new StandardKernel();

            kernel.Bind<JMLITSMEntities>().To<JMLITSMEntities>();
            kernel.Bind<IITSMRepository>().To<ITSMRepository>();
            kernel.Bind<JMLDirector>().To<JMLDirector>();
            kernel.Bind<FUS_SRMInterfaceStagingService>().To<FUS_SRMInterfaceStagingService>();
            kernel.Bind<JMLITSMSetting>().To<JMLITSMSetting>();

            kernel.Inject(this);
                                           
            this.AddTask(new TaskMethod(Initialise));
            this.AddTask(new TaskMethod(ProcessJMLMessages));
            this.CleanupTask = new TaskMethod(CleanupAll);
            this.ProcessError += new ProcessErrorDelegate(ITSMCompleteTicketProcess_ProcessError);
            this.ProcessStarted += new ProcessStartedDelegate(ITSMCompleteTicketProcess_ProcessStarted);
            this.ProcessStopped += new ProcessStoppedDelegate(ITSMCompleteTicketProcess_ProcessStopped);
            this.ProcessCleanupError += new ProcessErrorDelegate(ITSMCompleteTicketProcess_ProcessCleanupError);
        }

        private void InitProcess()
        {
            
        }

        

        private void ProcessJMLMessages()
        {
            int _waitMs;

            if (!int.TryParse(Settings.GetSettingValue(Wait_Complete_Ms, null), out _waitMs))
                _waitMs = WAIT_COMPLETE_MS_DEFAULT;

            while (true)
            {
                try
                {
                    Director.CompleteCreateRequests();
                }
                catch (Exception e)
                {
                    Log.LogException("Complete Create Requests Exception", e);
                }

                Thread.Sleep(_waitMs);
            }
        }

        private void ITSMCompleteTicketProcess_ProcessCleanupError(Exception ex)
        {
            Log.LogException("Process Cleanup Exception", ex);
        }

        private void ITSMCompleteTicketProcess_ProcessStarted()
        {
            Log.Process("Process Started", "Process started", System.Diagnostics.TraceEventType.Start);
        }

        private void ITSMCompleteTicketProcess_ProcessStopped()
        {
            Log.Process("Process Stopped", "Process Stopped", System.Diagnostics.TraceEventType.Stop);
        }

        private void ITSMCompleteTicketProcess_ProcessError(Exception ex)
        {
            // Called if Exception gets all the way to the top and kills the process...
            Log.LogException("Fatal Application Error", ex);
        }

        private void Initialise()
        {
           
        }

        private void CleanupAll()
        {
        }
    }
}
