﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Security.Principal;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;

namespace JML.ITSM.Logging
{
    public class Log
    {
        public const string LOG_PROCESS_CATEGORY = "Process";
        public const string LOG_AUDIT_CATEGORY = "Audit";
        public const string LOG_INFO_CATEGORY = "Information";
        public const string LOG_EXCEPTION_CATEGORY = "Exception";
        public const string LOG_AUDIT_LOGON_CATEGORY = "Audit Logon";
        public const string LOG_CRITICAL_EXCEPTION_POLICY = "Critical";
        public const string LOG_DEFAULT_EXCEPTION_POLICY = "Default";
        public const string LOG_TITLE = "JMLITSMInterface";

        static Log()
        {

            DatabaseFactory.SetDatabaseProviderFactory(new DatabaseProviderFactory());
            Logger.SetLogWriter(new LogWriterFactory().Create());
            IConfigurationSource config = ConfigurationSourceFactory.Create();
            ExceptionPolicyFactory factory = new ExceptionPolicyFactory(config);
            ExceptionPolicy.SetExceptionManager(factory.CreateManager());
           

        }

        public static void LogException(string logTitle, Exception ex)
        {
            LogException(logTitle, null, ex);
        }

        public static void LogException(string logTitle, string message, Exception ex)
        {
            StringBuilder sb = new StringBuilder();

            if (message != null)
            {
                sb.Append(message + "\n");
            }

            Exception thisEx = ex;
            do
            {
                sb.Append(ex.Message + "\n");
                sb.Append(ex.StackTrace + "\n");

                ex = ex.InnerException;

                if (ex != null)
                {
                    sb.Append("\nInner Exception:\n");
                }
            } while (ex != null);

            Logger.Write(sb.ToString(),
                LOG_EXCEPTION_CATEGORY,
                1, 1,
                System.Diagnostics.TraceEventType.Warning,
                logTitle);
        }

        public static void Info(string logTitle, string message)
        {
            Logger.Write(message,
                LOG_INFO_CATEGORY,
                4, 1,
                System.Diagnostics.TraceEventType.Information,
                logTitle);
        }

        public static void AuditLogon(string message, System.Diagnostics.TraceEventType eventType)
        {
            Dictionary<string, object> extendedProps = new Dictionary<string, object>();
            extendedProps.Add("AuditUser", WindowsIdentity.GetCurrent().Name);

            Logger.Write(message,
                LOG_AUDIT_LOGON_CATEGORY,
                1, 1,
                eventType,
                LOG_TITLE,
                extendedProps);
        }

        public static void Audit(string message)
        {
            Audit(LOG_TITLE, message, System.Diagnostics.TraceEventType.Information);
        }

        public static void Audit(string logTitle, string message)
        {
            Audit(logTitle, message, System.Diagnostics.TraceEventType.Information);
        }

        public static void Audit(string logTitle, string message, System.Diagnostics.TraceEventType eventType)
        {
            Dictionary<string, object> extendedProps = new Dictionary<string, object>();
            extendedProps.Add("AuditUser", WindowsIdentity.GetCurrent().Name);

            Logger.Write(message,
                LOG_AUDIT_CATEGORY,
                2, 1,
                eventType,
                logTitle,
                extendedProps);
        }

        public static void Process(string logTitle, string message, System.Diagnostics.TraceEventType eventType)
        {
            Dictionary<string, object> extendedProps = new Dictionary<string, object>();
            extendedProps.Add("AuditUser", WindowsIdentity.GetCurrent().Name);

            Logger.Write(message,
                LOG_PROCESS_CATEGORY,
                3, 1,
                eventType,
                logTitle,
                extendedProps);
        }

        public static void HandleException(Exception ex)
        {
            ExceptionPolicy.HandleException(ex, LOG_CRITICAL_EXCEPTION_POLICY);
        }

        public static void HandleException(Exception ex, string exceptionPolicy)
        {
            ExceptionPolicy.HandleException(ex, exceptionPolicy);
        }
    }
}
