﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

namespace JML.ITSM.Service
{
    public partial class JMLITSMService : ServiceBase
    {
        private JMLITSMProcess process = null;

        public JMLITSMService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            process = new JMLITSMProcess();
            process.Start(1000);
        }

        protected override void OnStop()
        {
            if (process != null && process.IsRunning)
            {
                process.Stop();
            }
        }

        #if (DEBUG)
        public void debug()
        {
            OnStart(null);
        }
        #endif
    }
}
