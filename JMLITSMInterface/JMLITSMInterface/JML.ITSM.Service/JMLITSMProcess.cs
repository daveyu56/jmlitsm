﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atos.Venom.Core.Process;
using System.IO;
using System.Net.Mail;
using JML.ITSM.Logging;
using JML.ITSM.BusinessLogic;
using JML.ITSM.DAL;
using JML.ITSM.Model;
using Ninject;
using JML.ITSM.WebServices.JMLITSMReference;


namespace JML.ITSM.Service
{
    public class JMLITSMProcess : Process
    {


        [Inject]
        public IITSMRepository Repository { get; set; }
       
        [Inject]
        public JMLITSMEntities Context {get; set;}
              
        [Inject]
        public ITSMDirector Director {get; set;}
        
        public JMLITSMProcess() 
            : base()
        {
            var kernel = new StandardKernel();

            kernel.Bind<JMLITSMEntities>().To<JMLITSMEntities>();
            kernel.Bind<IITSMRepository>().To<ITSMRepository>();
            kernel.Bind<JMLDirector>().To<JMLDirector>();
            kernel.Bind<FUS_SRMInterfaceStagingService>().To<FUS_SRMInterfaceStagingService>();
            kernel.Bind<JMLITSMSetting>().To<JMLITSMSetting>();

            kernel.Inject(this);
                                           
            this.AddTask(new TaskMethod(Initialise));
            this.AddTask(new TaskMethod(ProcessJMLMessages));
            this.CleanupTask = new TaskMethod(CleanupAll);
            this.ProcessError += new ProcessErrorDelegate(JMLITSMProcess_ProcessError);
            this.ProcessStarted += new ProcessStartedDelegate(JMLITSMProcess_ProcessStarted);
            this.ProcessStopped += new ProcessStoppedDelegate(JMLITSMProcess_ProcessStopped);
            this.ProcessCleanupError += new ProcessErrorDelegate(JMLITSMProcess_ProcessCleanupError);
        }

        private void InitProcess()
        {
            
        }

        

        private void ProcessJMLMessages()
        {
           
            Director.ProcessJMLMessages();
        }

        private void JMLITSMProcess_ProcessCleanupError(Exception ex)
        {
            Log.LogException("Process Cleanup Exception", ex);
        }

        private void JMLITSMProcess_ProcessStarted()
        {
            Log.Process("Process Started", "Process started", System.Diagnostics.TraceEventType.Start);
        }

        private void JMLITSMProcess_ProcessStopped()
        {
            Log.Process("Process Stopped", "Process Stopped", System.Diagnostics.TraceEventType.Stop);
        }

        private void JMLITSMProcess_ProcessError(Exception ex)
        {
            // Called if Exception gets all the way to the top and kills the process...
            Log.LogException("Fatal Application Error", ex);
        }

        private void Initialise()
        {
           
        }

        private void CleanupAll()
        {
        }
    }
}
