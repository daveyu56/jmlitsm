﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using JML.ITSM.BusinessLogic;
using JML.ITSM.DAL;
using JML.ITSM.Model;



namespace JML.ITSM.Service.Reporting
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {

          
           

           #if (DEBUG) 
            ReportingService service = new ReportingService();

              
               service.debug();

           #else
               ServiceBase[] ServicesToRun;
               ServicesToRun = new ServiceBase[] 
			   { 
				   new ReportingService() 
			   };
               ServiceBase.Run(ServicesToRun);
            #endif
        }
    }
}
