﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Atos.Venom.Core.Process;
using System.IO;
using System.Net.Mail;
using JML.ITSM.Logging;
using JML.ITSM.BusinessLogic;
using JML.ITSM.DAL;
using JML.ITSM.Model;
using Ninject;
using System.Threading;



namespace JML.ITSM.Service.Reporting
{
    public class ReportingProcess : Process
    {
        private DateTime lastTimeRan = DateTime.MinValue;

        [Inject]
        public IITSMRepository Repository { get; set; }
       
        [Inject]
        public JMLITSMEntities Context {get; set;}
              
        [Inject]
        public ReportingDirector Director {get; set;}

        [Inject]
        public SettingRepository Settings { get; set; }

        private readonly string Reporting_Day = "Reporting_Day";
        private readonly string Reporting_RunAt = "Reporting_RunAt";

        private bool hasRun = false;

        private readonly DateTime DEFAULT_RUNAT = new DateTime(1900, 1, 1, 9, 0, 0);

        public ReportingProcess() 
            : base()
        {
            var kernel = new StandardKernel();

            kernel.Bind<JMLITSMEntities>().To<JMLITSMEntities>();
            kernel.Bind<IITSMRepository>().To<ITSMRepository>();
            kernel.Bind<ReportingDirector>().To<ReportingDirector>();
            kernel.Bind<SettingRepository>().To<SettingRepository>();
            kernel.Bind<JMLITSMSetting>().To<JMLITSMSetting>();

            kernel.Inject(this);
                                           
            this.AddTask(new TaskMethod(Initialise));
            this.AddTask(new TaskMethod(CheckTime));
            this.CleanupTask = new TaskMethod(CleanupAll);
            this.ProcessError += new ProcessErrorDelegate(ReportingProcess_ProcessError);
            this.ProcessStarted += new ProcessStartedDelegate(ReportingProcess_ProcessStarted);
            this.ProcessStopped += new ProcessStoppedDelegate(ReportingProcess_ProcessStopped);
            this.ProcessCleanupError += new ProcessErrorDelegate(ReportingProcess_ProcessCleanupError);
        }

        private void InitProcess()
        {
            
        }

        private void CheckTime()
        {
            DateTime currentTime = DateTime.Now;
            DateTime rn = new DateTime(currentTime.Year, currentTime.Month, currentTime.Day, 0, 0, 0);

            int cd = currentTime.Day;

            DateTime sd = Repository.GetRunAt();
           

                 if (cd == 1 && rn > sd)
                 {
                     Log.Audit("CheckTime(): Running");
                     ProduceReport();
                     Repository.UpdateRunAt(rn);
                 }
           

            
            

        }

        private void ProduceReport()
        {
            try
            {
                Director.ProduceReport();
            }
            catch (Exception e)
            {
                Log.LogException("Produce Report Exception", e);
            }
        }

        private void ReportingProcess_ProcessCleanupError(Exception ex)
        {
            Log.LogException("Process Cleanup Exception", ex);
        }

        private void ReportingProcess_ProcessStarted()
        {
            Log.Process("Process Started", "Process started", System.Diagnostics.TraceEventType.Start);
        }

        private void ReportingProcess_ProcessStopped()
        {
            Log.Process("Process Stopped", "Process Stopped", System.Diagnostics.TraceEventType.Stop);
        }

        private void ReportingProcess_ProcessError(Exception ex)
        {
            // Called if Exception gets all the way to the top and kills the process...
            Log.LogException("Fatal Application Error", ex);
        }

        private void Initialise()
        {
           
        }

        private void CleanupAll()
        {
        }
    }
}
