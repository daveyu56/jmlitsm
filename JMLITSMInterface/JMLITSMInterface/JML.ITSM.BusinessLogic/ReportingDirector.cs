﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JML.ITSM.DAL.Datasources;
using JML.ITSM.Logging;
using JML.ITSM.Domain;
using JML.ITSM.Model;
using System.Data;
using ClosedXML.Excel;
using System.IO;
using JML.ITSM.WebServices;
using JML.ITSM.DAL;
using System.Net.Mail;

namespace JML.ITSM.BusinessLogic
{
    public class ReportingDirector
    {
        JMLITSMReportingDS _repository;
        SettingRepository _settings;

        private readonly string[] monthsArray = {"Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec" };

        private readonly string Reporting_EmailFromaddress = "Reporting_EmailFromaddress";
        private readonly string Reporting_EmailToaddress = "Reporting_EmailToaddress";
        private readonly string Reporting_EmailBody = "Reporting_EmailBody";
        private readonly string Reporting_EmailSubject = "Reporting_EmailSubject";
        private readonly string SMTP_CLIENT = "SMTP_CLIENT";

        public ReportingDirector()
        {
            _repository = new JMLITSMReportingDS();
            _settings = new SettingRepository();
        }

        public ReportingDirector(JMLITSMReportingDS repository, SettingRepository settings)
        {
            _repository = repository;
            _settings = settings;
        }



        public IList<JMLITSMReportData> GetReportingData(string month, string year)
        {
            int iMonth = int.Parse(month);
            int iYear = int.Parse(year);

            return _repository.GetReportData(iMonth, iYear);
        }

        public void ProduceReport()
        {
            DateTime today = DateTime.Today;

            int iMonth = today.AddMonths(-1).Month;

            int iYear = iMonth == 12 ? today.Year - 1 : today.Year;

            var reportData =_repository.GetReportData(iMonth, iYear);

            DataTable dt = PopulateDataTable(reportData);

            var wb = new XLWorkbook();
            wb.Worksheets.Add(dt);

            string fileName = string.Format("{0} - {1} {2}.xlsx", "DirX Generated JML-ITSM Interface Tickets", monthsArray[iMonth-1], iYear);

            EmailReport(wb, fileName);
           
        }

        private void EmailReport( XLWorkbook wb, string reportName)
        {
            try
            {

                using (MemoryStream ms = new MemoryStream())
                using (SmtpClient smtpClient = new SmtpClient(_settings.GetSettingValue(SMTP_CLIENT, null)))
                {
                    MailMessage mailMessage = new MailMessage(_settings.GetSettingValue(Reporting_EmailFromaddress, null), _settings.GetSettingValue(Reporting_EmailToaddress, null));

                    mailMessage.Subject = _settings.GetSettingValue(Reporting_EmailSubject, null);
                    mailMessage.Body = _settings.GetSettingValue(Reporting_EmailBody, null);

                    wb.SaveAs(ms);
                    ms.Seek(0, SeekOrigin.Begin);
                    Attachment attachment = new Attachment(ms, reportName, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    mailMessage.Attachments.Add(attachment);
                   
                    smtpClient.Send(mailMessage);

                }

            }
            catch (Exception e)
            {
                Log.LogException("Error Sending ITSM Interface Report", e);
            }
        }


       public DataTable PopulateDataTable(IList<JMLITSMReportData> exportData)
        {
            System.Data.DataTable dt = new System.Data.DataTable("ListViewData");

            dt.Columns.Add("Call Number");
            dt.Columns.Add("Order ID / External Call Ref.");
            dt.Columns.Add("Log DateTime");
            dt.Columns.Add("Ops Cat 1");
            dt.Columns.Add("Ops Cat 2");
            dt.Columns.Add("Ops Cat 3");
            dt.Columns.Add("CTI");
            dt.Columns.Add("Call Summary");

            foreach (var item in exportData)
            {
                dt.Rows.Add();

                dt.Rows[dt.Rows.Count - 1][0] = item.JMLReference;
                dt.Rows[dt.Rows.Count - 1][1] = item.InternalTicketId;
                dt.Rows[dt.Rows.Count - 1][2] = item.DateAdded.ToString();
                dt.Rows[dt.Rows.Count - 1][3] = item.Category;
                dt.Rows[dt.Rows.Count - 1][4] = item.Type;
                dt.Rows[dt.Rows.Count - 1][5] = item.Item;
                dt.Rows[dt.Rows.Count - 1][6] = item.OpCat;
                dt.Rows[dt.Rows.Count - 1][7] = item.Summary;

            }
            return dt;
        }

       
    }
}
