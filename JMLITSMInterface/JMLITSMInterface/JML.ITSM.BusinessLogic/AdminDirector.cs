﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JML.ITSM.DAL.Datasources;
using JML.ITSM.Domain;

namespace JML.ITSM.BusinessLogic
{
    public class AdminDirector
    {
        
        AdminDS _repository;

        public AdminDirector()
        {
            _repository = new AdminDS();
        }

        public AdminDirector(AdminDS repository)
        {
            _repository = repository;
        }

        public IList<ConfigurationData> GetConfigurationData()
        {
            return _repository.GetConfigurationData();
        }

        public void InsertConfiguraton (ConfigurationData config)
        {
            _repository.InsertConfiguration(config);
        }

        public void UpdateConfiguratonData(ConfigurationData config)
        {
            _repository.UpdateConfigurationData(config);
        }

        public void DeleteConfiguraton(ConfigurationData config)
        {
            _repository.DeleteConfiguration(config);
        }

        public IList<OpCatsData> GetOpCatsData()
        {
            return _repository.GetOpCatsData();
        }

        public void InsertOpCat (OpCatsData opCat)
        {
            _repository.InsertOpCat(opCat);
        }

        public void UpdateOpCat (OpCatsData opCat)
        {
            _repository.UpdateOpCat(opCat);
        }

        public void DeleteOpCat (OpCatsData opCat)
        {
            _repository.DeleteOpCat(opCat);
        }

        public IList<ImpactData> GetImpactData()
        {
            return _repository.GetImpactData();
        }

        public void InsertImpact(ImpactData impact)
        {
            _repository.InsertImpact(impact);
        }

        public void UpdateImpact(ImpactData impact)
        {
            _repository.UpdateImpact(impact);
        }

        public void DeleteImpact(ImpactData impact)
        {
            _repository.DeleteImpact(impact);
        }

        public IList<UrgencyData> GetUrgencyData()
        {
            return _repository.GetUrgencyData();
        }

        public void InsertUrgency(UrgencyData urgency)
        {
            _repository.InsertUrgency(urgency);
        }

        public void UpdateUrgency(UrgencyData urgency)
        {
            _repository.UpdateUrgency(urgency);
        }

        public void DeleteUrgency(UrgencyData urgency)
        {
            _repository.DeleteUrgency(urgency);
        }
    }
}
