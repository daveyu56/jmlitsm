﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JML.ITSM.DAL;
using JML.ITSM.Logging;
using JML.ITSM.Domain;
using JML.ITSM.Model;


namespace JML.ITSM.BusinessLogic
{
    public class JMLDirector : IDisposable
    {
        IJMLRepository _repository;
        SettingRepository _settings;

        private readonly string Service_Host_Url = "ServiceHostUrl";
        

        public JMLDirector(IJMLRepository repository, SettingRepository settings)
        {
            _repository = repository;
            _settings = settings;
           
               
        }

        public JMLDirector()
        {
            _repository = new JMLRepository();
            _settings = new SettingRepository();
        }

        public ProcessJMLMessageResponse SaveJMLRequest(JMLRequest request)
        {
           
            return _repository.SaveJMLRequest(request);
        }

        public void Dispose()
        {
            IDisposable disposable =_repository as IDisposable;
            disposable.Dispose();
            disposable = null;

            disposable = _settings as IDisposable;
            disposable.Dispose();
            disposable = null;

            this.Dispose();
        }

      

        public IList<IncomingMessage> GetAllJMLRequests()
        {
            return _repository.GetAllJMLRequests();
        }

        public IncomingMessage GetJMLRequestsById(int id)
        {
            return _repository.GetJMLRequestById(id);
        }


        public void LogResponse(JMLResponse response)
        {
            _repository.LogResponse(response);
        }

        public string GetHostServiceUrl()
        {
            return _settings.GetSettingValue(Service_Host_Url, string.Empty);
        }
    }
}
