﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JML.ITSM.DAL;
using JML.ITSM.Domain;
using JML.ITSM.Model;
using System.Transactions;
using System.Threading;
using System.Configuration;
using JML.ITSM.Logging;


namespace JML.ITSM.BusinessLogic
{
    public class ITSMDirector : IDisposable
    {
        IITSMRepository _repository;
        SettingRepository _settings;

        private const int TAKE_DEFAULT = 50;
     
      
        private const int DELAY_COMPLETE_MINS_DEFAULT = 15;

        private const string TICKET_STATUS_PLANNED = "Planning";
       
        private int _waitMs;
        private int _take;
        private int _delayMins;

        private const string OPEN_MESSAGE_TYPE = "OPEN";
        private const string UPDATE_MESSAGE_TYPE = "UPDATE";

        private readonly string ITSM_Request_UserName = "ITSMRequestUserName";
        private readonly string ITSM_Request_Password = "ITSMRequestPassword";

        private readonly string DEFAULT_WORK_INFO_TYPE = "General Information";
        private readonly string DEFAULT_WORK_INFO_COMM_SOURCE = "System Assignment";
        private readonly string DEFAULT_WORK_INFO_SUMMARY = "Check Attachment";
        private readonly string DEFAULT_WORK_INFO_DETAILS = "Attachment sent by external system";

        private readonly string Incoming_MessageQueue_Take = "IncomingMessageQueueTake";
      
    
        private readonly string Delay_Complete_Mins = "Delay_Complete_Mins";

        private readonly string COMPLETED_STATUS = "Completed";


        public ITSMDirector(IITSMRepository repository, SettingRepository settings)
        {
       
            _repository = repository;
            _settings = settings;
            
        }

      
        public void ProcessJMLMessages()
        {

       
            if (!int.TryParse(_settings.GetSettingValue(Incoming_MessageQueue_Take, null), out _take))
                _take = TAKE_DEFAULT;


            int skip = 0;
            int cnt = 0;

            var requests = _repository.GetJMLMessageQueueRequest(_take, skip);

            while (requests.Count > 0)
            {
                cnt++;
                ProcessJMLMessageBatch(requests);
                skip = _take*cnt;
                requests = _repository.GetJMLMessageQueueRequest(_take, skip);
            }

              
            
  
        }

        private void ProcessJMLMessageBatch(List<JMLRequest> requests)
        {
            List<string> failedOpenRequests = new List<string>();

            foreach (var request in requests)
            {
                switch (request.MessageType.ToUpper())
                {
                    case OPEN_MESSAGE_TYPE:
                        ProcessITSMOpenRequest(request, ref failedOpenRequests);
                        break;

                    case UPDATE_MESSAGE_TYPE:
                        if (!failedOpenRequests.Contains(request.InternalTicketId))
                            ProcessITSMUpdateRequest(request, string.Empty);
                        break;
                }

            }
        }

        private void ProcessITSMUpdateRequest(JMLRequest request, string status)
        {
           
            ITSMUpdateRequest itsmUpdateRequest = CreateITSMUpdateRequest(request, status);

            if (itsmUpdateRequest != null)
            {
                ITSMResponse response = null;

                try
                {
                    if (string.IsNullOrWhiteSpace(status))
                        _repository.UpdateIncomingMessageStatus(itsmUpdateRequest, null, (int)QueueStatusEnum.Queued, this.GetType().ToString());

                    response = _repository.PostRequestToITSM(itsmUpdateRequest);
                }
                catch (Exception e)
                {
                    Log.LogException("Process JML message exception", e);
                }

                if (string.IsNullOrWhiteSpace(status))
                {
                    if (response != null && response.ResultCode == "0")
                    {
                        _repository.UpdateIncomingMessageStatus(itsmUpdateRequest, response, (int)QueueStatusEnum.Processed, this.GetType().ToString());

                    }
                    else
                    {
                        _repository.UpdateIncomingMessageStatus(itsmUpdateRequest, response, (int)QueueStatusEnum.Unprocessed, this.GetType().ToString());
                    }
                }
                else if (response != null && response.ResultCode == "0")
                {
                    _repository.UpdateIncomingMessageStatus(itsmUpdateRequest, response, (int)QueueStatusEnum.Completed, this.GetType().ToString());

                    
                }


            }
        }

        private void ProcessITSMOpenRequest(JMLRequest request, ref List<string> failedOpenRequests)
        {
            ITSMOpenRequest itsmOpenRequest = CreateITSMOpenRequest(request);

            if (itsmOpenRequest != null)
            {
                ITSMResponse response = null;

                try
                {
                    _repository.UpdateIncomingMessageStatus(itsmOpenRequest, null, (int)QueueStatusEnum.Queued, this.GetType().ToString());

                    response = _repository.PostRequestToITSM(itsmOpenRequest);
                }
                catch (Exception e)
                {
                    Log.LogException("Process JML message exception", e);
                    response = null;
                }

                int newStatus;

                if (response != null && response.ResultCode == "0")
                {

                    newStatus =  (int)QueueStatusEnum.Processed;
                }
                else
                {
                    failedOpenRequests.Add(request.InternalTicketId);

                    newStatus = (int)QueueStatusEnum.Unprocessed;
                }

                _repository.UpdateIncomingMessageStatus(itsmOpenRequest, response, newStatus, this.GetType().ToString());

            }
 
        }

        private ITSMUpdateRequest CreateITSMUpdateRequest(JMLRequest request, string status)
        {
            ITSMUpdateRequest updateRequest = new ITSMUpdateRequest();

            updateRequest.Header.UserName = _settings.GetSettingValue(ITSM_Request_UserName, null);
            updateRequest.Header.Password = _settings.GetSettingValue(ITSM_Request_Password, null);

            updateRequest.SRNo = GetITSMReference(request.InternalTicketId);
            updateRequest.Body.JMLReferenceNo = request.InternalTicketId;
            updateRequest.Body.Impact = GetITSMImpact(request.Severity);
            updateRequest.Body.Urgency = GetITSMPriority(request.Priority);
            updateRequest.Body.Status = string.IsNullOrWhiteSpace(status) ? TICKET_STATUS_PLANNED : status;
            updateRequest.Body.WorkInfoType = (request.Attachment == string.Empty) ? string.Empty : DEFAULT_WORK_INFO_TYPE;
            updateRequest.Body.WorkInfoCommunicationSource = (request.Attachment == string.Empty) ? string.Empty : DEFAULT_WORK_INFO_COMM_SOURCE;
            updateRequest.Body.WorkInfoSummary = (request.Attachment == string.Empty) ? string.Empty : DEFAULT_WORK_INFO_SUMMARY;
            updateRequest.Body.WorkInfoDetails = (request.Attachment == string.Empty) ? string.Empty : DEFAULT_WORK_INFO_DETAILS;
            updateRequest.Body.Attachment1Name = request.AttachmentName;
            updateRequest.Body.Attachment1Data = request.Attachment;
            updateRequest.Body.AttachmentOrigSize = 0;
            updateRequest.Body.MasterId = request.MasterId;
            updateRequest.Body.TicketStatusId = 1;

            return updateRequest;
        }

        private string GetITSMReference(string internalTicketId)
        {
            return _repository.GetITSMReference(internalTicketId);
        }

        private string GetITSMPriority(string priority)
        {
            return _repository.GetITSMPriority(priority);
        }

        private string GetITSMImpact(string impact)
        {
            return _repository.GetITSMImpact(impact);
        }

        private ITSMOpenRequest CreateITSMOpenRequest(JMLRequest request)
        {
            ITSMOpenRequest openRequest = new ITSMOpenRequest();

            openRequest.Header.UserName = _settings.GetSettingValue(ITSM_Request_UserName, null);
            openRequest.Header.Password = _settings.GetSettingValue(ITSM_Request_Password, null);

            openRequest.Body.JMLReferenceNo = request.InternalTicketId;
            openRequest.Body.MessageId = request.MasterId.ToString();
            openRequest.SRDTitle = _repository.GetOpCats(request.Category, request.Type, request.Item);
            openRequest.Summary = request.ProblemTitle;
            openRequest.Company = "BBC";
            openRequest.SourceKeyword = "JML DirX";
            openRequest.Body.Status = TICKET_STATUS_PLANNED; //request.TicketStatus;
            openRequest.Body.Urgency = GetITSMPriority(request.Priority);
            openRequest.Body.Impact = GetITSMImpact(request.Severity);
            openRequest.Body.ShortDescription = request.ProblemText;
            openRequest.SubmitterId = request.UserLoginId;
            openRequest.RequestorId = request.UserLoginId;
            openRequest.Body.WorkInfoType = (request.Attachment == string.Empty) ? string.Empty : DEFAULT_WORK_INFO_TYPE;
            openRequest.Body.WorkInfoCommunicationSource = (request.Attachment == string.Empty) ? string.Empty : DEFAULT_WORK_INFO_COMM_SOURCE;
            openRequest.Body.WorkInfoSummary = (request.Attachment == string.Empty) ? string.Empty : DEFAULT_WORK_INFO_SUMMARY;
            openRequest.Body.WorkInfoDetails = (request.Attachment == string.Empty) ? string.Empty : DEFAULT_WORK_INFO_DETAILS;
            openRequest.Body.Attachment1Name = request.AttachmentName;
            openRequest.Body.Attachment1Data = request.Attachment;
            openRequest.Body.AttachmentOrigSize = 0;
            openRequest.Body.MasterId = request.MasterId;
            openRequest.Body.TicketStatusId = 1;
                                 
            return openRequest;
        }

      

        public void Dispose()
        {
            IDisposable disposable = _repository as IDisposable;
            disposable.Dispose();
            disposable = null;
            disposable = _settings as IDisposable;
            disposable.Dispose();
            disposable = null;
            this.Dispose();
        }





        public void CompleteCreateRequests()
        {
            

            if (!int.TryParse(_settings.GetSettingValue(Delay_Complete_Mins, null), out _delayMins))
                _delayMins = DELAY_COMPLETE_MINS_DEFAULT;

            var requests = _repository.GetCreateRequests(_delayMins);

            ProcessCreateRequestBatch(requests);

                
           
        }

        private void ProcessCreateRequestBatch(List<JMLRequest> requests)
        {
            foreach (var request in requests)
            {
                ProcessITSMUpdateRequest(request, COMPLETED_STATUS);
               
            }
        }

       
    }
}
