﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JML.ITSM.DAL.Datasources;
using JML.ITSM.Domain;

namespace JML.ITSM.BusinessLogic
{
    public class TicketDirector
    {
        
        TicketDS _repository;

        public TicketDirector()
        {
            _repository = new TicketDS();
        }

        public TicketDirector(TicketDS repository)
        {
            _repository = repository;
        }

        public IList<TicketRequestData> GetCreateTicketData()
        {
             return _repository.GetCreateTicketData();
        }

        public IList<TicketRequestData> GetTicketData()
        {
            return _repository.GetTicketData();
        }

        public IList<TicketResponseData> GetTicketResponseData(Guid masterId)
        {
            return _repository.GetTicketResponseData(masterId);
        }

        public object GetTicketDataByInternalTicketId(string internalTicketId)
        {
            return _repository.GetTicketDataByInternalTicketId(internalTicketId);
        }

        public IList<TicketDetailRequestData> GetTicketDetailData(string masterId)
        {
            return _repository.GetTicketDetailData(masterId);
        }
    }
}
