﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Owin;

namespace JML.ITSM.WebApp
{
    public class Startup
    {
       
       // Type valuesControllerType = typeof(WebApp.Controllers.JMLController);
               
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            HttpConfiguration config = new HttpConfiguration();

           
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                 name: "DefaultApi",
                 routeTemplate: "api/{controller}/{FaultMessage}",
                 defaults: new { FaultMessage = RouteParameter.Optional }
             );

            appBuilder.UseWebApi(config);
        }
    }
}
