﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Xml;
using JML.ITSM.WebApp.Models;
using JML.ITSM.Domain;
using JML.ITSM.BusinessLogic;
using JML.ITSM.DAL;
using JML.ITSM.Model;
using JML.ITSM.Logging;
using System.Xml.Serialization;
using System.IO;

namespace JML.ITSM.WebApp.Host
{
    public class JMLController : ApiController
    {
        IJMLRepository _repository;
        JMLITSMEntities _context;
        JMLDirector _director;

        private readonly string INVALID_JML_REQUEST_RECEIVED = "Invalid JML Request Received";
        private readonly string ACK_RESPONSE_MESSAGE_TYPE = "ACKNOWLEDGEMENT";

        public JMLController()
        {
            try
            {
                _context = new JMLITSMEntities();
                _repository = new JMLRepository();
                _director = new JMLDirector();
            }
            catch (Exception ex)
            {
                Log.LogException("JMLController failed in Constructor", ex);
            }
        }

        [HttpGet]
        public HttpResponseMessage Get()
        {
            FaultMessageResponse response = default(FaultMessageResponse);
            HttpResponseMessage resp = default(HttpResponseMessage);

            try
            {
                response = new FaultMessageResponse();
                resp = Request.CreateResponse<FaultMessageResponse>(HttpStatusCode.OK, response, Configuration.Formatters.XmlFormatter);

            }
            catch (Exception ex)
            {
                Log.LogException("JMLController failed in Get", ex);
            }

            return resp;
        }

        // POST api/<controller>
        [HttpPost]
       /* [RequireHttps] DEU TODO */
        public FaultMessageResponse Post(HttpRequestMessage request)
        {

       
            var xmlDoc = new XmlDocument();

  
            var ser = new XmlSerializer(typeof(FaultMessage));

            FaultMessage faultMessage = default(FaultMessage);                     

            try
            {
                  xmlDoc.Load(request.Content.ReadAsStreamAsync().Result);

                  faultMessage = ser.Deserialize(new StringReader(xmlDoc.OuterXml)) as FaultMessage;

                if (faultMessage != null)
                {
         

                    ProcessJMLMessageResponse response = ProcessJMLMessage(faultMessage, xmlDoc.OuterXml);

                    return SuccessResponse(faultMessage, response);

                    //HttpResponseMessage resp = Request.CreateResponse<FaultMessageResponse>(HttpStatusCode.OK, fmr, Configuration.Formatters.XmlFormatter);

                    //return resp;

                   // return SuccessResponse(faultMessage, response);
                }
                else
                {
                    //HttpResponseMessage resp = Request.CreateResponse<FaultMessageResponse>(HttpStatusCode.InternalServerError, faultMessage, Configuration.Formatters.XmlFormatter);

                    //return resp;
                    return FailedResponse(faultMessage, new Exception(INVALID_JML_REQUEST_RECEIVED));
                }
            }
            catch (ApplicationException ae)
            {
                //HttpResponseMessage resp = Request.CreateResponse<FaultMessageResponse> (HttpStatusCode.InternalServerError, faultMessage, Configuration.Formatters.XmlFormatter);

                //return resp;
                return FailedResponse(faultMessage, ae);
            }
            catch (WebException we)
            {
                //HttpResponseMessage resp = Request.CreateResponse<FaultMessageResponse>(HttpStatusCode.InternalServerError, faultMessage, Configuration.Formatters.XmlFormatter);

                //return resp;
                return FailedResponse(faultMessage, we);
            }
            catch (Exception e)
            {
                //HttpResponseMessage resp = Request.CreateResponse<FaultMessageResponse>(HttpStatusCode.InternalServerError, faultMessage, Configuration.Formatters.XmlFormatter);

                //return resp; 
                
                return FailedResponse(faultMessage, e);
            }
            finally
            {

                xmlDoc = null;
                ser = null;
                faultMessage = null;

                _context.Dispose();
                _context = null;

                _repository = null;
                _director = null;

            }


           




        }

      
        private ProcessJMLMessageResponse ProcessJMLMessage(FaultMessage faultMessage, string xml)
        {
            JMLRequest request = CreateRequestFromMessage(faultMessage, xml);

            return _director.SaveJMLRequest(request);


        }

        private JMLRequest CreateRequestFromMessage(FaultMessage faultMessage, string xml)
        {
            JMLRequest request = new JMLRequest();

            request.MessageType = BlankNullValues(faultMessage.Header.messageType);
            request.TicketId = BlankNullValues(faultMessage.Request.calldata.ticketID);
            request.ITSMTicketId = string.Empty;
            request.InternalTicketId = BlankNullValues(faultMessage.Request.calldata.extticket);
            request.MasterId = Guid.NewGuid();
            request.Priority = BlankNullValues(faultMessage.Request.calldata.priority);
            request.Severity = BlankNullValues(faultMessage.Request.calldata.severity);
            request.TicketStatus = BlankNullValues(faultMessage.Request.calldata.status);
            request.CallType = BlankNullValues(faultMessage.Request.calldata.callType);
            request.Source = BlankNullValues(faultMessage.Request.calldata.source);
            request.ProblemTitle = BlankNullValues(faultMessage.Request.calldata.problemtitle);
            request.ProblemText = BlankNullValues(faultMessage.Request.problemText);
            request.UserLoginId = BlankNullValues(faultMessage.Request.customerdata.GID);
            request.QueueStatusId = (int)QueueStatusEnum.Unprocessed;
            request.Category = BlankNullValues(faultMessage.Request.calldata.additionalAttributes[1].Value);
            request.Type = BlankNullValues(faultMessage.Request.calldata.additionalAttributes[2].Value);
            request.Item = BlankNullValues(faultMessage.Request.calldata.additionalAttributes[3].Value);

            if (faultMessage.Request.attachment != null)
            {
                request.Attachment = BlankNullValues(faultMessage.Request.attachment.Value);
                request.AttachmentName = BlankNullValues(faultMessage.Request.attachment.name);
                request.AttachmentType = BlankNullValues(faultMessage.Request.attachment.type);
            }
            else
            {
                request.Attachment = string.Empty;
                request.AttachmentName = string.Empty;
                request.AttachmentType = string.Empty;
            }


            request.Xml = BlankNullValues(xml);

            return request;
        }

        private string BlankNullValues(string instr)
        {
            return instr != null ? instr : string.Empty;
        }

        private FaultMessageResponse FailedResponse(FaultMessage faultMessage, Exception e)
        {
            FaultMessageResponse response = new FaultMessageResponse();

            Log.LogException("Failed Response Sent", e);

            response.Header.messageType = ACK_RESPONSE_MESSAGE_TYPE;
            response.Header.CRMName = faultMessage != null ? faultMessage.Header.CRMName : string.Empty;
            response.Header.messageID = string.Empty;

            response.Response.ticketID = faultMessage != null ? faultMessage.Request.calldata.ticketID : string.Empty;
            response.Response.extticket = string.Empty;
            response.Response.masterID = Guid.NewGuid();
            response.Response.errorCode = "-1";
            response.Response.resMessage = e.Message;

            _director.LogResponse(new JMLResponse(response.Header.messageType,
                                                  response.Header.CRMName,
                                                  response.Header.messageID,
                                                  response.Response.ticketID,
                                                  response.Response.extticket,
                                                  response.Response.masterID,
                                                  response.Response.errorCode,
                                                  response.Response.resMessage));


            return response;


        }

        private FaultMessageResponse SuccessResponse(FaultMessage faultMessage, ProcessJMLMessageResponse message)
        {
            FaultMessageResponse response = new FaultMessageResponse();

            response.Header.messageType = ACK_RESPONSE_MESSAGE_TYPE;
            response.Header.CRMName = faultMessage != null ? faultMessage.Header.CRMName : string.Empty;
            response.Header.messageID = string.Empty;

            response.Response.ticketID = faultMessage != null ? faultMessage.Request.calldata.ticketID : string.Empty;
            response.Response.extticket = message.InternalTicketId;
            response.Response.masterID = message.MasterId;
            response.Response.errorCode = "0";
            response.Response.resMessage = "OK";


            _director.LogResponse(new JMLResponse(response.Header.messageType,
                                                  response.Header.CRMName,
                                                  response.Header.messageID,
                                                  response.Response.ticketID,
                                                  response.Response.extticket,
                                                  response.Response.masterID,
                                                  response.Response.errorCode,
                                                  response.Response.resMessage));

            return response;
        }


    }


}
