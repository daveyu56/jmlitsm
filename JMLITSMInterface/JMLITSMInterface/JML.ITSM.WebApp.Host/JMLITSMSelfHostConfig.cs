﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.SelfHost;
using System.Web.Http.SelfHost.Channels;
using System.ServiceModel.Channels;

namespace JML.ITSM.WebApp.Host
{
    public class JMLITSMSelfHostConfig : HttpSelfHostConfiguration
    {
        public JMLITSMSelfHostConfig (string baseAddress) : base(baseAddress){}

        public JMLITSMSelfHostConfig (Uri baseAddress) : base(baseAddress) { }

        protected override BindingParameterCollection OnConfigureBinding(HttpBinding httpBinding)
        {
            // DEU TODO
            //httpBinding.Security.Mode = HttpBindingSecurityMode.Transport;
            httpBinding.Security.Mode = HttpBindingSecurityMode.None;
            return base.OnConfigureBinding(httpBinding);
        }
    }
}
