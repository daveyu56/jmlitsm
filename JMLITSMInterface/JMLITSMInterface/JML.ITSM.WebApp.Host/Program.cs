﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace JML.ITSM.WebApp.Host
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        public static void Main()
        {



            #if (DEBUG)

                JMLITSMHostService service = new JMLITSMHostService();


                service.debug();

            #else
                   ServiceBase[] ServicesToRun;
                   ServicesToRun = new ServiceBase[] 
			       { 
				       new JMLITSMHostService() 
			       };
                   ServiceBase.Run(ServicesToRun);
            #endif
        }
    }
}
