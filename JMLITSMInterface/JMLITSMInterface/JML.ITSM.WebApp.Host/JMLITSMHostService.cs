﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.SelfHost;
using JML.ITSM.BusinessLogic;


namespace JML.ITSM.WebApp.Host
{
    public partial class JMLITSMHostService : ServiceBase
    {

        private HttpSelfHostServer _server;
        public string _serviceAddress = string.Empty;

        public JMLITSMHostService()
        {
            InitializeComponent();

            try
            {
                JMLDirector _director = new JMLDirector();

                _serviceAddress = _director.GetHostServiceUrl();

              
            }
            catch (Exception e)
            {
                using (EventLog eventLog = new EventLog("Application"))
                {
                    eventLog.Source = "Application";
                    eventLog.WriteEntry(e.Message, EventLogEntryType.Error);
                    eventLog.WriteEntry(e.InnerException.Message, EventLogEntryType.Error);
                } 
            }

        }

        protected override void OnStart(string[] args)
        {
            var _config = new JMLITSMSelfHostConfig(_serviceAddress);

            _config.Formatters.Remove(_config.Formatters.JsonFormatter);

            _config.Formatters.XmlFormatter.UseXmlSerializer = true;

            _config.Routes.MapHttpRoute(
               name: "DefaultApi",
               routeTemplate: "api/{controller}/{FaultMessage}",
               defaults: new { FaultMessage = RouteParameter.Optional });

            _server = new HttpSelfHostServer(_config);
            _server.OpenAsync().Wait();

            using (EventLog eventLog = new EventLog("Application"))
            {
                eventLog.Source = "Application";
                eventLog.WriteEntry(string.Format("JML ITSM Host started Listening on {0}",  _serviceAddress), EventLogEntryType.Information);
            } 
           
        }

        protected override void OnStop()
        {
            _server.CloseAsync().Wait();
            _server.Dispose();
        }

        #if (DEBUG)
        public void debug()
        {
            OnStart(null);
        }
        #endif
    }
}
