﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JML.ITSM.Model;

namespace JML.ITSM.DAL
{
    public class CTIRepository
    {
        


        internal static string GetOpCatFromITSMRequestMessage(Guid guid)
        {
            string opCat;

            using (var context = new JMLITSMEntities())
            {
                var query = from p in context.ITSMRequestMessages
                            join op in context.OpCats on p.SRDTitleId equals op.id
                            where p.MasterId == guid
                            select op.Value;

                opCat = query.FirstOrDefault();
            
            }
            if (opCat != null)
                return opCat;
            else
                return string.Empty;




        }


        internal static string GetOpCatFromTicket(string category, string type, string item)
        {

            OpCat opCat;

            using (var context = new JMLITSMEntities())
            {
                var query = context.OpCats.Where(p=>p.Category == category && p.Type == type && p.Item == item);
                           
                          
                opCat = query.FirstOrDefault();

            }

            if (opCat != null)
                return opCat.Value;
            else
                return string.Empty;

        }
    }
}
