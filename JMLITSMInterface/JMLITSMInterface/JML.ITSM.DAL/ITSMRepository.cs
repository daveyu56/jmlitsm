﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Configuration;
using JML.ITSM.Domain;
using JML.ITSM.Model;
using System.Data.Entity;
using System.Data.Entity.Validation;
using Microsoft.Web.Services2.Security.X509;
using JML.ITSM.Logging;
using JML.ITSM.WebServices.JMLITSMReference;

namespace JML.ITSM.DAL
{
    public class ITSMRepository : IITSMRepository, IDisposable
    {
        private JMLITSMEntities _context;
        private FUS_SRMInterfaceStagingService _service;
        private SettingRepository _settings;

        private const string OPEN_MESSAGE_TYPE = "OPEN";


        public ITSMRepository(JMLITSMEntities context, SettingRepository settings, FUS_SRMInterfaceStagingService service)
        {
            _context = context;
            _service = service;
            _settings = settings;
        }

        public DateTime GetRunAt()
        {
            DateTime rd = new DateTime(2017, 8, 1, 0, 0, 0);

            var setting = _context.JMLITSMSettings.Where(p => p.SettingName.Equals("Reporting_RunAt")).FirstOrDefault();

            if (setting != null)
            {
                DateTime.TryParse(setting.SettingValue, out rd);
            }
          
            
            return rd;
        }

        public void UpdateRunAt(DateTime runDate)
        {
            var setting = _context.JMLITSMSettings.Where(p => p.SettingName.Equals("Reporting_RunAt")).FirstOrDefault();
            if (setting != null)
            {
                setting.SettingValue = new DateTime(runDate.Year, runDate.Month, runDate.Day, 0, 0, 0).ToString();
                _context.Entry(setting).State = EntityState.Modified;

                _context.SaveChanges();
            }
        }

        public List<JMLRequest> GetCreateRequests(int delayMins)
        {
            List<JMLRequest> createRequests = new List<JMLRequest>();

            DateTime now = DateTime.Now.AddMinutes(delayMins * -1); 
          
            // all un-completed open messages last modified less than delayMins minutes ago
            // 
            var query = _context.IncomingMessages.Where(t1 => t1.MessageType == OPEN_MESSAGE_TYPE && t1.QueueStatusId == (int)QueueStatusEnum.Processed && t1.Date_Last_Modified < now).OrderBy(t1 => t1.Date_Added);
          
            foreach (var r in query)
            {
                
                JMLRequest request = PopulateJMLRequest(r);

                createRequests.Add(request);
            }

            return createRequests;
        }

        public ITSMResponse PostRequestToITSM(ITSMUpdateRequest request)
        {
            string resultMessage = string.Empty;
            bool hasAttachment = false;
            byte[] attachmentData = null;
            string attachmentName = string.Empty;
            int attachmentSize = 0;
           
            try
            {
               

                SetupService(request, _service);

                RequestHasAttachment(request, ref hasAttachment, ref attachmentData, ref attachmentName, ref attachmentSize);

                LogITSMRequest(request);

                var itsmResponse = _service.UpdateRequest (request.SRNo,
                                                           request.Body.JMLReferenceNo,
                                                           request.Body.MasterId.ToString(),
                                                           request.Body.Impact,
                                                           request.Body.Urgency,
                                                           request.Body.Status,
                                                           request.Body.WorkInfoType,
                                                           request.Body.WorkInfoCommunicationSource,
                                                           request.Body.WorkInfoSummary,
                                                           request.Body.WorkInfoDetails,
                                                           attachmentName,
                                                           attachmentData,
                                                           attachmentSize,
                                                           hasAttachment,
                                                           out resultMessage);

                ITSMResponse response = new ITSMResponse()
                {
                    ResultCode = itsmResponse,
                    ResultMessage = resultMessage
                };

                LogITSMResponse(response, request.Body.MasterId);

               

                return response;

            }
            catch (Exception e)
            {
                Log.LogException("Error Sending ITSM Update Request", e);
                return null;
            }



        }

        private static void RequestHasAttachment(ITSMRequest request, ref bool hasAttachment, ref byte[] attachmentData, ref string attachmentName, ref int attachmentSize)
        {
            if (!string.IsNullOrWhiteSpace(request.Body.Attachment1Data))
            {

                attachmentData = Convert.FromBase64CharArray(request.Body.Attachment1Data.ToCharArray(), 0, request.Body.Attachment1Data.ToCharArray().Length);
                attachmentName = request.Body.Attachment1Name;
                attachmentSize = attachmentData.Length;
                hasAttachment = true;
            }
        }

        public ITSMResponse PostRequestToITSM(ITSMOpenRequest request)
        {

           
            string resultMessage = string.Empty;
         
            bool hasAttachment = false;
            byte[] attachmentData=null;
            string attachmentName = string.Empty;
            int attachmentSize = 0;

            FUS_SRMInterfaceStagingService _service = new FUS_SRMInterfaceStagingService();

            try
            {
                SetupService(request, _service);

                RequestHasAttachment(request, ref hasAttachment, ref attachmentData, ref attachmentName, ref attachmentSize);

                LogITSMRequest(request);



                var itsmResponse = _service.CreateRequest(request.Body.JMLReferenceNo,
                                                         request.Body.MasterId.ToString(),
                                                         request.SRDTitle,
                                                         request.Summary,
                                                         request.Company,
                                                         request.SourceKeyword,
                                                         request.Body.Status,
                                                         request.Body.Urgency,
                                                         request.Body.Impact,
                                                         request.Body.ShortDescription,
                                                         request.SubmitterId,
                                                         request.RequestorId,
                                                         request.Body.WorkInfoType,
                                                         request.Body.WorkInfoCommunicationSource,
                                                         request.Body.WorkInfoSummary,
                                                         request.Body.WorkInfoDetails,
                                                         attachmentName,
                                                         attachmentData,
                                                         attachmentSize,
                                                         hasAttachment,
                                                         out resultMessage);

                   

                    ITSMResponse response = new ITSMResponse()
                    {
                        ResultCode = itsmResponse,
                        ResultMessage = resultMessage
                    };

                    LogITSMResponse(response, request.Body.MasterId);

                    return response;
                
            }
            catch (Exception e)
            {
                
                Log.LogException("Error Sending ITSM Create Request", e);
                return null;
            }
           

            
        }

        private void goGet()
        {
           
           string soap = @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:urn=""urn:FUS_SRMInterfaceStaging"">
   <soapenv:Header>
      <urn:AuthenticationInfo>
         <urn:userName>DIRXADMIN</urn:userName>
         <urn:password>DIRXADMIN</urn:password>
         <!--Optional:-->
         <urn:authentication>?</urn:authentication>
         <!--Optional:-->
         <urn:locale>?</urn:locale>
         <!--Optional:-->
         <urn:timeZone>?</urn:timeZone>
      </urn:AuthenticationInfo>
   </soapenv:Header>
   <soapenv:Body>
      <urn:CreateRequest>
         <!--Optional:-->
         <urn:ReferenceNo>HD9010000505</urn:ReferenceNo>
         <!--Optional:-->
         <urn:MessageID>M0001</urn:MessageID>
         <!--Optional:-->
         <urn:SRDTitle>SRD_JML_IDAM_BulkUpdate</urn:SRDTitle>
         <!--Optional:-->
         <urn:Summary>test create from soapUI</urn:Summary>
         <!--Optional:-->
         <urn:Company>BBC</urn:Company>
         <!--Optional:-->
         <urn:SourceKeyword>JML DirX</urn:SourceKeyword>
         <!--Optional:-->
         <urn:Status>Pending</urn:Status>
         <!--Optional:-->
         <urn:Urgency>3-Medium</urn:Urgency>
         <!--Optional:-->
         <urn:Impact>3-Moderate/Limited</urn:Impact>
         <!--Optional:-->
         <urn:ShortDescription>SRD_JML_IDAM_BulkUpdate test incident details</urn:ShortDescription>
         <!--Optional:-->
         <urn:SubmitterID>JAMES.DICKER</urn:SubmitterID>
         <!--Optional:-->
         <urn:RequesterID>MIKE.WYSOCKI</urn:RequesterID>
         <!--Optional:-->
         <urn:WorkInfoType>General Information</urn:WorkInfoType>
         <!--Optional:-->
         <urn:WorkInfoCommunicationSource>System Assignment</urn:WorkInfoCommunicationSource>
         <!--Optional:-->
         <urn:WorkInfoSummary>Check Attachment</urn:WorkInfoSummary>
         <!--Optional:-->
         <urn:WorkInfoDetails>Attachment sent by external system</urn:WorkInfoDetails>
         <!--Optional:-->
         <urn:Attachment1Name>?</urn:Attachment1Name>
         <!--Optional:-->
         <urn:Attachment1Data>cid:768862168152</urn:Attachment1Data>
         <!--Optional:-->
         <urn:Attachment1OrigSize>?</urn:Attachment1OrigSize>
      </urn:CreateRequest>
   </soapenv:Body>
</soapenv:Envelope>";


           



            HttpWebRequest request = (HttpWebRequest)WebRequest.Create( _settings.GetSettingValue("ServiceUrl", null));

            request.Credentials = CredentialCache.DefaultCredentials;

            string certName = _settings.GetSettingValue("CertificateName", null);

            X509CertificateStore store = X509CertificateStore.LocalMachineStore(X509CertificateStore.MyStore);

            store.OpenRead();

            X509CertificateCollection col = (X509CertificateCollection)store.FindCertificateBySubjectString(certName);

            X509Certificate cert = col[0];

            request.ClientCertificates.Add(cert);
                       
         //   request.Headers.Add("SOAPAction"urn:FUS_SRMInterfaceStaging/CreateRequest\"");
            request.ContentType = "text/xml;charset=\"utf-8\"";
            request.Accept = "text/xml";
            request.Method = "POST";

            using (Stream stm = request.GetRequestStream())
            {
                 using (StreamWriter stmw = new StreamWriter(stm))
                 {
                      stmw.Write(soap);
                 }
            }

            WebResponse response = request.GetResponse();

            Stream responseStream = response.GetResponseStream();
           
          
        }

       

        private void SetupService(ITSMRequest request, FUS_SRMInterfaceStagingService _service)
        {
            string certName = _settings.GetSettingValue("CertificateName", null);

            X509CertificateStore store = X509CertificateStore.LocalMachineStore(X509CertificateStore.MyStore);

            store.OpenRead();

            X509CertificateCollection col = (X509CertificateCollection)store.FindCertificateBySubjectString(certName);

            X509Certificate cert = col[0];

            _service.ClientCertificates.Add(cert);

            _service.UseDefaultCredentials = true;


            _service.Url = _settings.GetSettingValue("ServiceUrl", null);

            _service.AuthenticationInfoValue = new AuthenticationInfo();
            _service.AuthenticationInfoValue.userName = request.Header.UserName;
            _service.AuthenticationInfoValue.password = request.Header.Password;
        }

            
        public List<JMLRequest> GetJMLMessageQueueRequest(int take, int skip)
        {
            List<JMLRequest> requests = new List<JMLRequest>();
                        
            //var query  = _context.IncomingMessages.Where(p => p.QueueStatusId == (int)QueueStatusEnum.Unprocessed).OrderBy(p=>p.MessageType).ThenBy(p=>p.Date_Added).Skip(skip).Take(take).ToList();

            var query = _context.IncomingMessages.Where(p => p.id == 400154).ToList();
            foreach (var result in query)
            {
                JMLRequest request = PopulateJMLRequest(result);

               
                    requests.Add(request);
              

            }

            return requests;
        }

        public string GetITSMReference(string internalTicketId)
        {
            string itsmReference = string.Empty;

            var ticket = _context.IncomingMessages.Where(p => p.InternalTicketId == internalTicketId && p.MessageType == OPEN_MESSAGE_TYPE).FirstOrDefault();

            if (ticket != null)
            {
                itsmReference = ticket.ITSMTicketId;
            }

            return itsmReference;

        }


        private static JMLRequest PopulateJMLRequest(IncomingMessage result)
        {
            JMLRequest request = new JMLRequest();

            request.MessageType = result.MessageType;

            request.TicketId = result.MessageType;
            request.ITSMTicketId = result.ITSMTicketId;
            request.InternalTicketId = result.InternalTicketId;
            request.Priority = result.Priority;
            request.Severity = result.Severity;
            request.TicketStatus = result.TicketStatu.Status;
            request.CallType = result.CallType;
            request.Source = result.Source;
            request.ProblemTitle = result.ProblemTitle;
            request.ProblemText = result.ProblemText;
            request.UserLoginId = result.UserLoginId;
            request.QueueStatusId = result.QueueStatusId;
            request.DateAdded = result.Date_Added;
            request.AddedBy = result.Added_By;
            request.DateLastModified = result.Date_Last_Modified;
            request.LastModifiedBy = result.Last_Modified_By;
            request.Attachment = result.Attachment;
            request.AttachmentName = result.AttachmentName;
            request.AttachmentType = result.AttachmentType;
            request.Category = result.Category;
            request.Type = result.Type;
            request.Item = result.Item;
            request.MasterId = result.MasterId;
            return request;
        }

        public string GetOpCats(string category, string type, string item)
        {
            var opCat = _context.OpCats.Where(p => p.Category == category && p.Type == type && p.Item == item).FirstOrDefault();

            return opCat == null ? string.Empty : opCat.Value;
        }

        public void UpdateIncomingMessageStatus (ITSMRequest request, ITSMResponse response, int status, string modBy)
        {
         
            var message = _context.IncomingMessages.Where(p => p.MasterId == request.Body.MasterId).FirstOrDefault();

            if (message != null)
            {
                if (response != null && response.ResultCode == "0")
                    message.ITSMTicketId = response.ResultMessage;

                message.QueueStatusId = status;

                if (status != (int)QueueStatusEnum.Processed || status != (int)QueueStatusEnum.Completed)
                    message.SentToITSMCount++;

                message.Last_Modified_By = modBy;
                message.Date_Last_Modified = DateTime.Now;

                _context.Entry(message).State = EntityState.Modified;

                _context.SaveChanges();
            }
        }

        private void LogITSMRequest (ITSMRequest request)
        {
            ITSMRequestMessage itsmRequest = new ITSMRequestMessage();

            List<KeyValuePair<string, string>> args = new List<KeyValuePair<string, string>>();

            args.Add(new KeyValuePair<string, string>("JMLReferenceNo", request.Body.JMLReferenceNo));
            args.Add(new KeyValuePair<string, string>("MasterId", request.Body.MasterId.ToString()));  
            args.Add(new KeyValuePair<string, string>("MessageId", request.Body.MessageId));
            args.Add(new KeyValuePair<string, string>("ShortDescription", request.Body.ShortDescription));
            args.Add(new KeyValuePair<string, string>("Impact", request.Body.Impact));
            args.Add(new KeyValuePair<string, string>("Urgency", request.Body.Urgency));
            args.Add(new KeyValuePair<string, string>("Status", request.Body.Status));
            args.Add(new KeyValuePair<string, string>("WorkInfoType", request.Body.WorkInfoType));
            args.Add(new KeyValuePair<string, string>("WorkInfoCommunicationSource", request.Body.WorkInfoCommunicationSource));
            args.Add(new KeyValuePair<string, string>("WorkInfoSummary", request.Body.WorkInfoSummary));
            args.Add(new KeyValuePair<string, string>("WorkInfoDetails", request.Body.WorkInfoDetails));
            args.Add(new KeyValuePair<string, string>("Attachment1Name", request.Body.Attachment1Name));
            args.Add(new KeyValuePair<string, string>("Attachment1Data", request.Body.Attachment1Data));
            args.Add(new KeyValuePair<string, string>("AttachmentOrigSize", request.Body.AttachmentOrigSize.ToString()));                 

            if (typeof(ITSMOpenRequest) == request.GetType())
            {
                ITSMOpenRequest openRequest = request as ITSMOpenRequest;

                args.Add(new KeyValuePair<string, string>("SRDTitle", openRequest.SRDTitle));         
                args.Add(new KeyValuePair<string, string>("Summary", openRequest.Summary));     
                args.Add(new KeyValuePair<string, string>("Company", openRequest.Company));     
                args.Add(new KeyValuePair<string, string>("SourceKeyword", openRequest.SourceKeyword));     
                args.Add(new KeyValuePair<string, string>("SubmitterId", openRequest.SubmitterId));     
                args.Add(new KeyValuePair<string, string>("RequestorId", openRequest.RequestorId));     
                
                Logging.Log.Audit(Logging.Formatters.FormatMessage(args, typeof(ITSMOpenRequest).Name));

               
                itsmRequest.MasterId = openRequest.Body.MasterId;
                itsmRequest.SRDTitleId = GetSRDTitleId(openRequest.SRDTitle);
                itsmRequest.Summary = openRequest.Summary;
                itsmRequest.Company = openRequest.Company;
                itsmRequest.SourceKeyword = openRequest.SourceKeyword;
                itsmRequest.StatusId = GetStatusId(openRequest.Body.Status);
                itsmRequest.Urgency = openRequest.Body.Urgency;
                itsmRequest.Impact = openRequest.Body.Impact;
                itsmRequest.ShortDescription = openRequest.Body.ShortDescription;
                itsmRequest.SubmitterId = openRequest.SubmitterId;
                itsmRequest.RequesterId = openRequest.RequestorId;
                itsmRequest.WorkInfoType = openRequest.Body.WorkInfoType;
                itsmRequest.WorkInfoSummary = openRequest.Body.WorkInfoSummary;
                itsmRequest.WorkInfoDetails = openRequest.Body.WorkInfoDetails;
                itsmRequest.WorkInfoCommSrc = openRequest.Body.WorkInfoCommunicationSource;
                itsmRequest.Attachment1Data = openRequest.Body.Attachment1Data;
                itsmRequest.Attachment1Name = openRequest.Body.Attachment1Name;
                itsmRequest.AttachmentOrigSize = openRequest.Body.AttachmentOrigSize;
                itsmRequest.TicketStatusId = (int)QueueStatusEnum.Unprocessed;
                
               

                          

            }
            else if (typeof(ITSMUpdateRequest) == request.GetType())
            {
                ITSMUpdateRequest updateRequest = request as ITSMUpdateRequest;

                args.Add(new KeyValuePair<string, string>("SRNo", updateRequest.SRNo));     

                Logging.Log.Audit(Logging.Formatters.FormatMessage(args, typeof(ITSMUpdateRequest).Name));

               
                itsmRequest.MasterId = updateRequest.Body.MasterId;
                itsmRequest.SRDTitleId = null;
                itsmRequest.Summary = string.Empty;
                itsmRequest.Company = "BBC";
                itsmRequest.SourceKeyword = "JML DirX";
                itsmRequest.StatusId = GetStatusId(updateRequest.Body.Status);
                itsmRequest.Urgency = updateRequest.Body.Urgency;
                itsmRequest.Impact = updateRequest.Body.Impact;
                itsmRequest.ShortDescription = updateRequest.Body.ShortDescription;
                itsmRequest.SubmitterId = string.Empty;
                itsmRequest.RequesterId = string.Empty;
                itsmRequest.WorkInfoType = updateRequest.Body.WorkInfoType;
                itsmRequest.WorkInfoSummary = updateRequest.Body.WorkInfoSummary;
                itsmRequest.WorkInfoDetails = updateRequest.Body.WorkInfoDetails;
                itsmRequest.WorkInfoCommSrc = updateRequest.Body.WorkInfoCommunicationSource;
                itsmRequest.Attachment1Data = updateRequest.Body.Attachment1Data;
                itsmRequest.Attachment1Name = updateRequest.Body.Attachment1Name;
                itsmRequest.AttachmentOrigSize = updateRequest.Body.AttachmentOrigSize;
                itsmRequest.TicketStatusId = (int)QueueStatusEnum.Unprocessed;
              
            }
            else
            {
                throw new ApplicationException ("Invalid ITSM Request Type");
            }

            itsmRequest.Date_Added = DateTime.Now;
            itsmRequest.Added_By = "JML.ITSM.Interface.Service";

            try
            {
                _context.Entry(itsmRequest).State = EntityState.Added;

                _context.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                var errorMessages = ex.EntityValidationErrors
                    .SelectMany(x => x.ValidationErrors)
                    .Select(x => x.ErrorMessage);

                var fullErrorMessage = string.Join("; ", errorMessages);
                throw;
            }

           
        }

        private int GetStatusId(string status)
        {
            var stat = _context.TicketStatus.Where(p => p.Status.ToUpper() == status.ToUpper()).FirstOrDefault();

            return stat.TicketStatusId;
        }

        private int? GetSRDTitleId(string srdTitle)
        {
            var opCat = _context.OpCats.Where(p => p.Value.ToUpper() == srdTitle.ToUpper()).FirstOrDefault();

            if (opCat != null)
            {
                return opCat.id;
            }
            else
            {
                return null;
            }
           
        }

        private void LogITSMResponse (ITSMResponse response, Guid masterId)
        {

            List<KeyValuePair<string, string>> args = new List<KeyValuePair<string, string>>();
            args.Add(new KeyValuePair<string, string>("ResultCode", response.ResultCode));
            args.Add(new KeyValuePair<string, string>("ResultMessage", response.ResultMessage));
            args.Add(new KeyValuePair<string, string>("MasterId", masterId.ToString()));


            ITSMResponseMessage itsmResponse = new ITSMResponseMessage();

            itsmResponse.ResultCode = response.ResultCode;
            itsmResponse.ResultMessage = response.ResultMessage;
            itsmResponse.MasterId = masterId;

            itsmResponse.Date_Added = DateTime.Now;
            itsmResponse.Added_By = "JML.ITSM.Interface.Service";

            _context.Entry(itsmResponse).State = EntityState.Added;

            var request = _context.ITSMRequestMessages.Where(p => p.MasterId == masterId).FirstOrDefault();
            if (request != null)
            {
                request.TicketStatusId = (int)QueueStatusEnum.Processed;
                _context.Entry(request).State = EntityState.Modified;
            }

            _context.SaveChanges();

            Logging.Log.Audit(Logging.Formatters.FormatMessage(args, typeof(ITSMResponse).Name));
        }

        public string GetITSMPriority (string jmlPriority)
        {
            var itsmPriority = _context.Urgencies.Where(p => p.JMLUrgency.ToUpper() == jmlPriority.ToUpper()).FirstOrDefault();

            if (itsmPriority != null)
            {
                return itsmPriority.ITSMUrgency;
            }
            else
            {
                return jmlPriority;
            }
        }

        public string GetITSMImpact(string jmlImpact)
        {
            var itsmImpact = _context.Impacts.Where(p => p.JMLImpact.ToUpper() == jmlImpact.ToUpper()).FirstOrDefault();

            if (itsmImpact != null)
            {
                return itsmImpact.ITSMImpact;
            }
            else
            {
                return jmlImpact;
            }
        }

     
        public void Dispose()
        {

            Dispose(true);
           // GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {

            if (disposing)
            {

                IDisposable disposable = _context as IDisposable;
                disposable.Dispose();
                disposable = null;

                disposable = _settings as IDisposable;
                disposable.Dispose();
                disposable = null;

            }
           
        }
    }
}
