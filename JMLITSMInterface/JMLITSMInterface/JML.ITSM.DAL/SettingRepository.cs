﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JML.ITSM.Model;

namespace JML.ITSM.DAL
{

    public class SettingRepository : IDisposable
    {
        JMLITSMEntities _context;

        private static readonly IDictionary<string, string> DefaultSettings = new Dictionary<string, string>()
        {
                {"IncomingMessageQueueTake", "50"},
                {"WaitMs", "12000"},
                {"Wait_Complete_Ms", "12000"},
                {"Delay_Complete_Mins", "15"},
                {"ITSMRequestUserName", "DIRXADMIN"},
                {"ITSMRequestPassword", "DIRXADMIN"},
                {"CertificateName", @"itsmuk.it-solutions.atos.net"},
                {"Reporting_EmailFromaddress", @"donotreply@atos.net"},
                {"Reporting_EmailToaddress", @"andy.colthorpe@atos.net,philmore.douglas@atos.net,david.urquhart@atos.net"},
                {"Reporting_EmailBody", @"JML-ITSM interface report"},
                {"Reporting_EmailSubject", @"JML-ITSM interface report"},
                {"Reporting_Day", "1"},
                {"Reporting_RunAt", @"01/01/1900 09:00:00"},
                {"SMTP_CLIENT", @"smtp.national.bbc.co.uk"},
                {"ServiceUrl", @"https://itsmuk.it-solutions.atos.net/arsys/services/ARService?server=itsmuk-ar.siemens-it-solutions.com&webService=FUS_SRMInterfaceStaging"}

        };

        public SettingRepository(JMLITSMEntities context)
        {
            _context = context;
           
        }

        public SettingRepository()
        {
            _context = new JMLITSMEntities();
        }

        public string GetSettingValue (string settingName, string settingType)
        {
            if (string.IsNullOrWhiteSpace(settingType))
                settingType = string.Empty;

            if (string.IsNullOrWhiteSpace(settingName))
                settingName = string.Empty;

            var setting = _context.JMLITSMSettings.Where(p => p.SettingName.ToUpper() == settingName.ToUpper() && p.SettingType.ToUpper() == settingType.ToUpper()).FirstOrDefault();

            if (setting != null)
            {
                return setting.SettingValue;
            }
            else
            {
                return DefaultSettings[settingName];
            }
        }

        public void Dispose()
        {

            Dispose(true);
          //  GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {

            if (disposing)
            {

                IDisposable disposable = _context as IDisposable;
                disposable.Dispose();
                disposable = null;
            }
        }
    }
}
