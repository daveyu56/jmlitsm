﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using JML.ITSM.Domain;
using JML.ITSM.Model;


namespace JML.ITSM.DAL
{
    public interface IITSMRepository
    {
        ITSMResponse PostRequestToITSM(ITSMOpenRequest request);

        List<JMLRequest> GetJMLMessageQueueRequest(int take, int skip);

        void UpdateIncomingMessageStatus(ITSMRequest request, ITSMResponse response, int status, string modifiedBy);

        string GetOpCats(string category, string type, string item);

        ITSMResponse PostRequestToITSM(ITSMUpdateRequest itsmUpdateRequest);

        string GetITSMPriority(string priority);

        string GetITSMImpact(string impact);

        List<JMLRequest> GetCreateRequests(int delayMins);


        string GetITSMReference(string internalTicketId);

        

        void UpdateRunAt(DateTime runDate);

        DateTime GetRunAt();
    }
}
