﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JML.ITSM.Model;
using JML.ITSM.Domain;
using JML.ITSM.Logging;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace JML.ITSM.DAL
{
    public class JMLRepository : IJMLRepository, IDisposable
    {
        JMLITSMEntities _context;

        Object lockObj = new Object();

        private readonly string OPEN_MESSAGE_TYPE = "OPEN";
        private readonly string JMLITSMInterface = "JML.ITSM.Interface";

        public JMLRepository(JMLITSMEntities context)
        {
            _context = context;
               
        }

        public JMLRepository()
        {
            _context = new JMLITSMEntities();
        }

        public ProcessJMLMessageResponse SaveJMLRequest(JMLRequest request)
        {
            string internalTicketId = string.Empty;

            try
            {
                if (request.MessageType.ToUpper() == OPEN_MESSAGE_TYPE)
                    internalTicketId = GenerateInternalTicketId();
                else
                    internalTicketId = request.InternalTicketId;

                IncomingMessage message = new IncomingMessage();

                message.MessageType = request.MessageType;
                message.TicketId = request.TicketId;
                message.ITSMTicketId = request.ITSMTicketId;
                message.InternalTicketId = internalTicketId;
                message.MasterId = Guid.NewGuid();
                message.Priority = request.Priority;
                message.Severity = request.Severity;
                message.TicketStatusId = GetTicketStatusIdFor(request.TicketStatus);
                message.CallType = request.CallType;
                message.Source = request.Source;
                message.ProblemTitle = request.ProblemTitle;
                message.ProblemText = request.ProblemText;
                message.UserLoginId = request.UserLoginId;
                message.QueueStatusId = (int)QueueStatusEnum.Unprocessed;
                message.Date_Added = DateTime.Now;
                message.Added_By = JMLITSMInterface;
                message.Date_Last_Modified = null;
                message.Last_Modified_By = string.Empty;
                message.Attachment = request.Attachment;
                message.AttachmentName = request.AttachmentName;
                message.AttachmentType = request.AttachmentType;
                message.Category = request.Category;
                message.Type = request.Type;
                message.Item = request.Item;
                message.SentToITSMCount = 0;
                message.Date_Added = DateTime.Now;
                message.Added_By = "JML.ITSM.Interface.WebApp";
                message.Xml = request.Xml;

                _context.Entry(message).State = System.Data.Entity.EntityState.Added;

                _context.SaveChanges();

                LogJMLMessage(message);

                return new ProcessJMLMessageResponse()
                {
                    InternalTicketId = internalTicketId,
                    MasterId = message.MasterId
                };
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
                throw new ApplicationException("Database Error saving JML request", dbEx);
            }
            catch (Exception e)
            {
                throw new ApplicationException("Database Error saving JML request", e);
            }
        }

       

        private void LogJMLMessage(IncomingMessage message)
        {
            List<KeyValuePair<string, string>> args = new List<KeyValuePair<string, string>>();
            args.Add(new KeyValuePair<string, string>("MessageType", message.MessageType));
            args.Add(new KeyValuePair<string, string>("TicketId", message.TicketId));
            args.Add(new KeyValuePair<string, string>("ITSMTicketId", message.ITSMTicketId));
            args.Add(new KeyValuePair<string, string>("InternalTicketId", message.InternalTicketId));
            args.Add(new KeyValuePair<string, string>("Priority", message.Priority));
            args.Add(new KeyValuePair<string, string>("Severity", message.Severity));
            args.Add(new KeyValuePair<string, string>("TicketStatusId", message.TicketStatusId.ToString()));
            args.Add(new KeyValuePair<string, string>("CallType", message.CallType));
            args.Add(new KeyValuePair<string, string>("Source", message.TicketId));
            args.Add(new KeyValuePair<string, string>("ProblemTitle", message.ProblemTitle));
            args.Add(new KeyValuePair<string, string>("ProblemText", message.ProblemText));
            args.Add(new KeyValuePair<string, string>("UserLoginId", message.UserLoginId));
            args.Add(new KeyValuePair<string, string>("QueueStatusId", message.QueueStatusId.ToString()));
            args.Add(new KeyValuePair<string, string>("Attachment", message.Attachment));
            args.Add(new KeyValuePair<string, string>("Date_Added", message.Date_Added.ToString()));
            args.Add(new KeyValuePair<string, string>("Added_By", message.Added_By));
            args.Add(new KeyValuePair<string, string>("Date_Last_Modified",  message.Date_Last_Modified != null ? message.Date_Last_Modified.ToString() : string.Empty));
            args.Add(new KeyValuePair<string, string>("Last_Modified_By", message.Last_Modified_By));
          
            Logging.Log.Audit(Logging.Formatters.FormatMessage(args, typeof(IncomingMessage).Name));

        }

        private string GenerateInternalTicketId()
        {
           
            InternalTicketId ticketId = new InternalTicketId();

            lock (lockObj)
            {
                _context.Entry(ticketId).State = System.Data.Entity.EntityState.Added;
                _context.SaveChanges();
            }

            return string.Format("HD{0:0000000000}", ticketId.TicketId);
          
        }

        private int GetTicketStatusIdFor(string status)
        {
            var ticketStatusId = _context.TicketStatus.Where(p => p.Status.ToUpper() == status.Trim().ToUpper()).FirstOrDefault();
            return ticketStatusId.TicketStatusId;
        }

        public void LogResponse(JMLResponse response)
        {
            JMLITSMResponse jmlitsmResponse = new JMLITSMResponse();

            jmlitsmResponse.MessageType = response.MessageType;
            jmlitsmResponse.CRMName = response.CRMName;
            jmlitsmResponse.MessageId = response.MessageId;
            jmlitsmResponse.TicketId = response.TicketId;
            jmlitsmResponse.InternalTicketId = response.Extticket;
            jmlitsmResponse.MasterId = response.MasterId;
            jmlitsmResponse.ResponseCode = response.ErrorCode;
            jmlitsmResponse.ResponseMessage = response.ResMessage;
            jmlitsmResponse.Date_Added = DateTime.Now;
            jmlitsmResponse.Added_By = this.GetType().ToString();
           
            _context.Entry(jmlitsmResponse).State = System.Data.Entity.EntityState.Added;

            _context.SaveChanges();

        }                 

        public IList<IncomingMessage> GetAllJMLRequests()
        {
            var requests = _context.IncomingMessages.AsNoTracking().ToList();

            return requests;
        }

        public IncomingMessage GetJMLRequestById(int id)
        {
            var request = _context.IncomingMessages.Where(p => p.id == id).AsNoTracking().FirstOrDefault();

            return request;
        }


        public void Dispose()
        {

            Dispose(true);
          //  GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {

            if (disposing)
            {

                IDisposable disposable = _context as IDisposable;
                disposable.Dispose();
                disposable = null;

            }
           
        }
    }
}
