﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JML.ITSM.Domain;
using JML.ITSM.Model;



namespace JML.ITSM.DAL.Datasources
{
    public class TicketDS
    {
        private readonly string JML_OPEN_MESSAGE = "OPEN";
      
        public TicketDS()
        {
        }


        public IList<TicketDetailRequestData> GetTicketDetailData(string masterId)
        {
            IList<TicketDetailRequestData> ticketDetail = new List<TicketDetailRequestData>();

            using (var context = new JMLITSMEntities())
            {
                var ticket = context.IncomingMessages.AsNoTracking().Where(p=>p.MasterId.ToString() == masterId).FirstOrDefault();

                if (ticket != null)
                {
                    TicketDetailRequestData item = new TicketDetailRequestData();

                    item.JMLReference = ticket.TicketId;
                    item.InternalTicketId = ticket.InternalTicketId;
                    item.ITSMReference = ticket.ITSMTicketId;
                    item.LoginId = ticket.UserLoginId;
                    item.DateAdded = ticket.Date_Added;
                    item.OpCat1 = ticket.Category;
                    item.OpCat2 = ticket.Type;
                    item.OpCat3 = ticket.Item;
                    item.CTI = CTIRepository.GetOpCatFromITSMRequestMessage(ticket.MasterId);
                    item.Summary = ticket.ProblemTitle;
                    item.TicketStatus = ticket.QueueStatu.Status;
                    item.MasterId = ticket.MasterId;

                    item.MessageType = ticket.MessageType;
                    item.Priority = ticket.Priority;
                    item.Severity = ticket.Severity;
                    item.CallType = ticket.CallType;
                    item.Source = ticket.Source;
                    item.ProblemText = ticket.ProblemText;
                    item.QueueStatus = ticket.QueueStatu.Status;
                    item.LastModified = ticket.Date_Last_Modified;
                    item.LastModifiedBy = ticket.Last_Modified_By;
                    item.AttachmentName = ticket.AttachmentName;
                    item.AttachmentType = ticket.AttachmentType;
                    item.SentToITSMCount = ticket.SentToITSMCount.GetValueOrDefault();

                    ticketDetail.Add(item);

                }

             
            }

            return ticketDetail;
        }

        public IList<TicketRequestData> GetTicketData()
        {
            List<TicketRequestData> ticketData = new List<TicketRequestData>();

            using (var context = new JMLITSMEntities())
            {
                var tickets = context.IncomingMessages.AsNoTracking().OrderBy(p => p.TicketId).ToList();

                foreach (var ticket in tickets)
                {
                    ProcessTicketData(ticketData, ticket);

                }
            }

            return ticketData;
        }

        public IList<TicketRequestData> GetTicketDataByInternalTicketId (string internalTicketId)
        {
            List<TicketRequestData> ticketData = new List<TicketRequestData>();

            using (var context = new JMLITSMEntities())
            {
                var tickets = context.IncomingMessages.AsNoTracking().Where(p=>p.InternalTicketId == internalTicketId).OrderBy(p => p.TicketId).ToList();

                foreach (var ticket in tickets)
                {
                    ProcessTicketData(ticketData, ticket);

                }
            }

            return ticketData;
        }

        private static void ProcessTicketData(List<TicketRequestData> ticketData, IncomingMessage ticket)
        {
            TicketRequestData item = new TicketRequestData();
            item.JMLReference = ticket.TicketId;
            item.InternalTicketId = ticket.InternalTicketId;
            item.ITSMReference = ticket.ITSMTicketId;
            item.LoginId = ticket.UserLoginId;
            item.DateAdded = ticket.Date_Added;
            item.OpCat1 = ticket.Category;
            item.OpCat2 = ticket.Type;
            item.OpCat3 = ticket.Item;
            item.CTI = CTIRepository.GetOpCatFromITSMRequestMessage(ticket.MasterId);
            item.Summary = ticket.ProblemTitle;
            item.TicketStatus = ticket.QueueStatu.Status;
            item.MasterId = ticket.MasterId;

            ticketData.Add(item);
        }

        public IList<TicketResponseData> GetTicketResponseData(Guid masterId)
        {
            List <TicketResponseData> responseList = new List<TicketResponseData>();

            using (var context = new JMLITSMEntities())
            {
                var responses = context.ITSMResponseMessages.AsNoTracking().Where(p => p.MasterId == masterId).ToList();

                foreach (var response in responses)
                {
                    TicketResponseData responseData = new TicketResponseData();
                    responseData.ResultCode = response.ResultCode;
                    responseData.ResultMessage = response.ResultMessage;
                    responseData.DateAdded = response.Date_Added;

                    responseList.Add(responseData);
                }
            }

            return responseList;
        }

       


        public IList<TicketRequestData> GetCreateTicketData()
        {
            List<TicketRequestData> ticketData = new List<TicketRequestData>();

            using (var context = new JMLITSMEntities())
            {
                var tickets = context.IncomingMessages.AsNoTracking().Where(p=>p.MessageType == JML_OPEN_MESSAGE).OrderBy(p => p.TicketId).ToList();

                foreach (var ticket in tickets)
                {
                    ProcessTicketData(ticketData, ticket);

                }
            }

            return ticketData;
        }
    }
}
