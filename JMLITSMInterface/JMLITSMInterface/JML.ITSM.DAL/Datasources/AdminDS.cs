﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JML.ITSM.Domain;
using JML.ITSM.Model;



namespace JML.ITSM.DAL.Datasources
{
    public class AdminDS
    {


        public AdminDS()
        {
        }

        public IList<ConfigurationData> GetConfigurationData()
        {

            List<ConfigurationData> data = new List<ConfigurationData>();

            using (var context = new JMLITSMEntities())
            {
                var configs = context.JMLITSMSettings.AsNoTracking().ToList();

                foreach (var config in configs)
                {
                    ProcessConfigurationData(data, config);

                }
            }

            return data;
        }

        private void ProcessConfigurationData(List<ConfigurationData> data, JMLITSMSetting config)
        {
            ConfigurationData item = new ConfigurationData();

            item.Id = config.id;
            item.SettingName = config.SettingName;
            item.SettingValue = config.SettingValue;

            data.Add(item);
        }

        public void UpdateConfigurationData(ConfigurationData config)
        {
            using (var context = new JMLITSMEntities())
            {
                var item = context.JMLITSMSettings.Find(config.Id);

                if (item != null)
                {

                    item.SettingName = config.SettingName;
                    item.SettingValue = config.SettingValue;

                    context.Entry(item).State = System.Data.Entity.EntityState.Modified;

                    context.SaveChanges();
                }
            }
        }

        public void InsertConfiguration (ConfigurationData config)
        {
            using (var context = new JMLITSMEntities())
            {
                JMLITSMSetting item = new JMLITSMSetting();

                item.SettingName = config.SettingName;
                item.SettingType = string.Empty;
                item.SettingValue = config.SettingValue;

                context.Entry(item).State = System.Data.Entity.EntityState.Added;

                context.SaveChanges();

            }
        }


        public void DeleteConfiguration (ConfigurationData config)
        {
            using (var context = new JMLITSMEntities())
            {

                var item = context.JMLITSMSettings.Find(config.Id);

                if (item != null)
                {

                    context.Entry(item).State = System.Data.Entity.EntityState.Deleted;

                    context.SaveChanges();
                }

            }
        }
      




        public IList<UrgencyData> GetUrgencyData()
        {

            List<UrgencyData> data = new List<UrgencyData>();

            using (var context = new JMLITSMEntities())
            {
                var urgencies = context.Urgencies.AsNoTracking().ToList();

                foreach (var urgency in urgencies)
                {
                    ProcessUrgencyData(data, urgency);

                }
            }

            return data;
        }

        private void ProcessUrgencyData(List<UrgencyData> data,Urgency urgency)
        {
            UrgencyData item = new UrgencyData();

            item.Id = urgency.id;
            item.JMLUrgency = urgency.JMLUrgency;
            item.ITSMUrgency = urgency.ITSMUrgency;

            data.Add(item);
        }

        public void UpdateUrgency (UrgencyData urgency)
        {
            using (var context = new JMLITSMEntities())
            {
                var item = context.Urgencies.Find(urgency.Id);

                if (item != null)
                {

                    item.JMLUrgency= urgency.JMLUrgency;
                    item.ITSMUrgency = urgency.ITSMUrgency;

                    context.Entry(item).State = System.Data.Entity.EntityState.Modified;

                    context.SaveChanges();
                }
            }
        }

        public void InsertUrgency (UrgencyData urgency)
        {
            using (var context = new JMLITSMEntities())
            {
                Urgency item = new Urgency();

                item.JMLUrgency= urgency.JMLUrgency;
                item.ITSMUrgency = urgency.ITSMUrgency;

                context.Entry(item).State = System.Data.Entity.EntityState.Added;

                context.SaveChanges();

            }
        }


        public void DeleteUrgency (UrgencyData urgency)
        {
            using (var context = new JMLITSMEntities())
            {

                var item = context.Urgencies.Find(urgency.Id);

                if (item != null)
                {

                    context.Entry(item).State = System.Data.Entity.EntityState.Deleted;

                    context.SaveChanges();
                }

            }
        }
      

        public IList<ImpactData> GetImpactData()
        {

            List<ImpactData> data = new List<ImpactData>();

            using (var context = new JMLITSMEntities())
            {
                var impacts = context.Impacts.AsNoTracking().ToList();

                foreach (var impact in impacts)
                {
                    ProcessImpactsData(data, impact);

                }
            }

            return data;
        }

        public void UpdateImpact(ImpactData impact)
        {
            using (var context = new JMLITSMEntities())
            {
                var item = context.Impacts.Find(impact.Id);

                if (item != null)
                {

                    item.JMLImpact = impact.JMLImpact;
                    item.ITSMImpact = impact.ITSMImpact;

                    context.Entry(item).State = System.Data.Entity.EntityState.Modified;

                    context.SaveChanges();
                }
            }
        }

        public void InsertImpact(ImpactData impact)
        {
            using (var context = new JMLITSMEntities())
            {
                Impact item = new Impact();

                item.JMLImpact = impact.JMLImpact;
                item.ITSMImpact = impact.ITSMImpact;


                context.Entry(item).State = System.Data.Entity.EntityState.Added;

                context.SaveChanges();

            }
        }


        public void DeleteImpact(ImpactData impact)
        {
            using (var context = new JMLITSMEntities())
            {

                var item = context.Impacts.Find(impact.Id);

                if (item != null)
                {

                    context.Entry(item).State = System.Data.Entity.EntityState.Deleted;

                    context.SaveChanges();
                }

            }
        }

        private void ProcessImpactsData(List<ImpactData> data, Impact impact)
        {
            ImpactData item = new ImpactData();

            item.Id = impact.id;
            item.JMLImpact = impact.JMLImpact;
            item.ITSMImpact = impact.ITSMImpact;

            data.Add(item);
        }

        public IList<OpCatsData> GetOpCatsData()
        {

            List<OpCatsData> data = new List<OpCatsData>();

            using (var context = new JMLITSMEntities())
            {
                var opCats = context.OpCats.AsNoTracking().ToList();

                foreach (var opCat in opCats)
                {
                    ProcessOpCatsData(data, opCat);

                }
            }

            return data;
        }

        public void UpdateOpCat (OpCatsData opCat)
        {
            using (var context = new JMLITSMEntities())
            {
                var item = context.OpCats.Find(opCat.Id);

                if (item != null)
                {
                    item.Category = opCat.Category;
                    item.Type = opCat.Type;
                    item.Item = opCat.Item;
                    item.Value = opCat.Title;

                    context.Entry(item).State = System.Data.Entity.EntityState.Modified;

                    context.SaveChanges();
                }
            }
        }

        public void InsertOpCat (OpCatsData opCat)
        {
            using (var context = new JMLITSMEntities())
            {
                OpCat item = new OpCat();

                item.Category = opCat.Category;
                item.Type = opCat.Type;
                item.Item = opCat.Item;
                item.Value = opCat.Title;

                context.Entry(item).State = System.Data.Entity.EntityState.Added;

                context.SaveChanges();
            
            }
        }


        public void DeleteOpCat (OpCatsData opCat)
        {
            using (var context = new JMLITSMEntities())
            {
              
                 var item = context.OpCats.Find(opCat.Id);

                if (item != null)
                {

                    context.OpCats.Remove(item);
                    context.SaveChanges();
                }
            
            }
        }

        private void ProcessOpCatsData(List<OpCatsData> data, OpCat opCat)
        {
            OpCatsData item = new OpCatsData();

            item.Id = opCat.id;
            item.Category = opCat.Category;
            item.Type = opCat.Type;
            item.Item = opCat.Item;
            item.Title = opCat.Value;

            data.Add(item);
        }

      
    }
}
