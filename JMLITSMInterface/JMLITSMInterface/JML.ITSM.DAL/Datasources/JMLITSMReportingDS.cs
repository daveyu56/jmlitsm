﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JML.ITSM.Domain;
using JML.ITSM.Model;




namespace JML.ITSM.DAL.Datasources
{
    public class JMLITSMReportingDS
    {

        private static readonly int JML_START_YEAR = 2012;

        public static List<int> ReportingYear { get; set; }

       
        public JMLITSMReportingDS()
        {
           
        }

        public IList<JMLITSMReportData> GetReportData()
        {
            List<JMLITSMReportData> reportData = new List<JMLITSMReportData>();

            using (var context = new JMLITSMEntities())
            {
                var tickets = context.IncomingMessages.AsNoTracking().OrderBy(p => p.TicketId).ToList();

                foreach (var ticket in tickets)
                {
                    JMLITSMReportData item = new JMLITSMReportData();
                    item.JMLReference = ticket.TicketId;
                    item.InternalTicketId = ticket.InternalTicketId;
                    item.DateAdded = ticket.Date_Added;
                    item.Category = ticket.Category;
                    item.Type = ticket.Type;
                    item.Item = ticket.Item;
                    item.OpCat = CTIRepository.GetOpCatFromITSMRequestMessage(ticket.MasterId);
                    item.Summary = ticket.ProblemTitle;


                    reportData.Add(item);

                }
            }

            return reportData;
        }

        public static IList <int> GetReportingYears()
        {
            ReportingYear = new List<int>();

            int currentYear = DateTime.Now.Year;

            for (int i = JML_START_YEAR; i <= currentYear; i++)
            {
                ReportingYear.Add(i);
            }

            return ReportingYear;
        }

     

        public IList<JMLITSMReportData> GetReportData(int month, int year)
        {
            List<JMLITSMReportData> reportData = new List<JMLITSMReportData>();

            using (var context = new JMLITSMEntities())
            {
                var tickets = context.IncomingMessages.AsNoTracking().Where(p=>p.MessageType == "OPEN").OrderBy(p => p.TicketId).ToList();

                foreach (var ticket in tickets)
                {
                    DateTime? dateAdded = ticket.Date_Added;

                    if (dateAdded.HasValue && dateAdded.Value.Year == year && dateAdded.Value.Month == month)
                    {

                        JMLITSMReportData item = new JMLITSMReportData();
                        item.JMLReference = ticket.TicketId;
                        item.InternalTicketId = ticket.InternalTicketId;
                        item.DateAdded = ticket.Date_Added;
                        item.Category = ticket.Category;
                        item.Type = ticket.Type;
                        item.Item = ticket.Item;
                        item.OpCat = CTIRepository.GetOpCatFromTicket(item.Category, item.Type, item.Item);
                        item.Summary = ticket.ProblemTitle;


                        reportData.Add(item);
                    }

                }
            }

            return reportData;
        }
    }
}
