﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JML.ITSM.Domain;
using JML.ITSM.Model;


namespace JML.ITSM.DAL
{
    public interface IJMLRepository
    {
        ProcessJMLMessageResponse SaveJMLRequest(JMLRequest request);

        IList<IncomingMessage> GetAllJMLRequests();

        IncomingMessage GetJMLRequestById(int id);



        void LogResponse(JMLResponse response);
    }
}
