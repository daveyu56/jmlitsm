﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JML.ITSM.Domain
{
    public class ITSMUpdateRequest : ITSMRequest
    {
        public ITSMUpdateRequest()
        {
            Header = new ITSMRequestHeader();
            Body = new ITSMRequestBody();
        }

        public string SRNo { get; set; }
    }
}
