﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JML.ITSM.Domain
{

    public class ITSMRequest
    {
        public ITSMRequestHeader Header {get; set;}

        public ITSMRequestBody Body {get; set; }

        
    }

}
