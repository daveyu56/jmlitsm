﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JML.ITSM.Domain
{
    public class ITSMResponse
    {
        public string ResultCode { get; set; }

        public string ResultMessage { get; set; }
    }
}
