﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JML.ITSM.Domain
{
    public enum QueueStatusEnum {Unprocessed = 1, Queued, Processed, Completed};

    public class JMLRequest
    {
        public int Id { get; set; }

        public string MessageType {get; set;}

        public string TicketId { get; set; }

        public string ITSMTicketId { get; set; }

        public string InternalTicketId { get; set; }

        public Guid MasterId { get; set; }

        public string Priority { get; set; }

        public string Severity { get; set; }

        public string TicketStatus { get; set; }

        public string CallType { get; set; }

        public string Source { get; set; }

        public string ProblemTitle { get; set; }

        public string Attachment { get; set; }

        public string AttachmentName { get; set; }

        public string AttachmentType { get; set; }

        public string ProblemText { get; set; }

        public string UserLoginId { get; set; }

        public int QueueStatusId { get; set; }

        public string Category { get; set; }

        public string Type { get; set; }

        public string Item { get; set; }

        public DateTime? DateAdded { get; set; }

        public string AddedBy { get; set; }

        public DateTime? DateLastModified { get; set; }

        public string LastModifiedBy { get; set; }

        public string Xml { get; set; }
    }
}
