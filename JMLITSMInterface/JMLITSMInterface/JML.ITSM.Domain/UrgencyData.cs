﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JML.ITSM.Domain
{
    public class UrgencyData
    {
        public int Id { get; set; }
        public string JMLUrgency { get; set; }
        public string ITSMUrgency { get; set; }
      
    }
}
