﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JML.ITSM.Domain
{
    public class ITSMOpenRequest : ITSMRequest
    {

        public ITSMOpenRequest()
        {
            Header = new ITSMRequestHeader();
            Body = new ITSMRequestBody();

        }
        public string SRDTitle { get; set; }

        public string Summary { get; set; }

        public string Company { get; set; }

        public string SourceKeyword { get; set; }

        public string SubmitterId { get; set; }

        public string RequestorId { get; set; }
    }
}
