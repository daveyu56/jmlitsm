﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JML.ITSM.Domain
{
    public class JMLITSMReportData
    {
        public string JMLReference { get; set; }
        public string InternalTicketId { get; set; }
        public DateTime? DateAdded { get; set; }
        public string Category { get; set; }
        public string Type { get; set; }
        public string Item { get; set; }
        public string OpCat { get; set; }
        public string Summary { get; set; }
    }
}
