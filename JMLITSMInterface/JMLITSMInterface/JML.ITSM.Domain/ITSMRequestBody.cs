﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JML.ITSM.Domain
{
    public class ITSMRequestBody
    {
       

        public string JMLReferenceNo { get; set; }

        public string MessageId { get; set; }

        public string Impact { get; set; }

        public string Urgency { get; set; }

        public string Status { get; set; }

        public string WorkInfoType { get; set; }

        public string WorkInfoCommunicationSource { get; set; }

        public string WorkInfoSummary { get; set; }

        public string WorkInfoDetails { get; set; }

        public string Attachment1Name { get; set; }

        public string Attachment1Data { get; set; }

        public int AttachmentOrigSize { get; set; }

        public string ShortDescription { get; set; }

        public Guid MasterId { get; set; }

        public int TicketStatusId { get; set; }

    }
}
