﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JML.ITSM.Domain
{
    public class ProcessJMLMessageResponse
    {
        public string InternalTicketId { get; set; }
        public Guid MasterId { get; set; }
    }
}
