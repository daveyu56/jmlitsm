﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JML.ITSM.Domain
{
    public class OpCatsData
    {
        public int Id { get; set; }
        public string Category { get; set; }
        public string Type { get; set; }
        public string Item { get; set; }
        public string Title { get; set; }
    }
}
