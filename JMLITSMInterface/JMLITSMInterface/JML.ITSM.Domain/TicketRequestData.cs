﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JML.ITSM.Domain
{
    public class TicketRequestData
    {
        public string JMLReference { get; set; }
        public string InternalTicketId { get; set; }
        public string ITSMReference { get; set; }
        public string LoginId { get; set; }
        public string TicketStatus { get; set; }
        public string Summary { get; set; }
        public DateTime? DateAdded { get; set; }
        public string OpCat1 { get; set; }
        public string OpCat2 { get; set; }
        public string OpCat3 { get; set; }
        public string CTI { get; set; }
        public Guid MasterId { get; set; }


        public TicketRequestData()
        {
            
        }
    }
}
