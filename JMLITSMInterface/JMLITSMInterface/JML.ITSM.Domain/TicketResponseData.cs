﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JML.ITSM.Domain
{
    public class TicketResponseData
    {
        public string ResultCode { get; set; }
        public string ResultMessage { get; set; }
        public DateTime? DateAdded { get; set; }

        public TicketResponseData()
        {

        }


    }
}
