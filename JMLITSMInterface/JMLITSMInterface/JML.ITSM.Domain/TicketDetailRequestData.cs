﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JML.ITSM.Domain
{
    public class TicketDetailRequestData : TicketRequestData
    {
        public string MessageType { get; set; }
        public string Priority { get; set; }
        public string Severity { get; set; }
        public string CallType { get; set; }
        public string Source { get; set; }
        public string ProblemText {get; set;}
        public string QueueStatus { get; set; }
        public DateTime? LastModified { get; set; }
        public string LastModifiedBy { get; set; }
        public string AttachmentName { get; set; }
        public string AttachmentType { get; set; }
        public int SentToITSMCount { get; set; }
    }
}
