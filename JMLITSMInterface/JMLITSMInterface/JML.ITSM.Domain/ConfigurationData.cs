﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JML.ITSM.Domain
{
    public class ConfigurationData
    {
        public int Id { get; set; }
        public string SettingName { get; set; }
        public string SettingValue { get; set; }

    }
}
