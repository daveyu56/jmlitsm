﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JML.ITSM.Domain
{
    public class JMLResponse
    {
         public string MessageType { get; set; }

      
        public string MessageId { get; set; }

       
        public string CRMName { get; set; }

        public string TicketId { get; set; }

      
        public string Extticket { get; set; }

       
        public Guid? MasterId { get; set; }

      
        public string ErrorCode { get; set; }

      
        public string ResMessage { get; set; }

        public JMLResponse(string messageType, string crmName, string messageId, string ticketId, string extTicketId, Guid masterId, string errorCode, string resMessage)
        {

            MessageType = messageType;
            MessageId = messageId;
            CRMName = crmName;
            TicketId = ticketId;
            Extticket = extTicketId;
            MasterId = masterId;
            ErrorCode = errorCode;
            ResMessage = resMessage;
        }
                                         
                                                       
       
    }
}
