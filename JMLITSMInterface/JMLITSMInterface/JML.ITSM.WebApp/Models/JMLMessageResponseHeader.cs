﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace JML.ITSM.WebApp.Models
{
    public class JMLMessageResponseHeader
    {
        [DataMember(Order=0)]
        public string messageType { get; set; }

        [DataMember(Order=1)]
        public string messageID { get; set; }

        [DataMember(Order=2)]
        public string CRMName { get; set; }
    }
}
