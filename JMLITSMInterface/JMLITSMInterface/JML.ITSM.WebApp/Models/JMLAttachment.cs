﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Xml.Serialization;

namespace JML.ITSM.WebApp.Models
{
    public class JMLAttachment
    {
        [XmlAttribute]
        public string type { get; set; }

        [XmlAttribute]
        public string name { get; set; }

        [XmlText]
        public string Value { get; set; }
    }
}
