﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace JML.ITSM.WebApp.Models
{
   
    public class JMLAdditionalAttribute
    {
        [XmlAttribute("name")]
        public string name { get; set; }

        [XmlText]
        public string Value { get; set; }
    }
}
