﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace JML.ITSM.WebApp.Models
{
    public class JMLMessageResponse
    {
        [DataMember(Order=0)]
        public string ticketID { get; set; }

        [DataMember(Order=1)]
        public string extticket { get; set; }

        [DataMember(Order=2)]
        public Guid masterID { get; set; }

        [DataMember(Order=3)]
        public string errorCode { get; set; }

        [DataMember(Order=4)]
        public string resMessage { get; set; }
    }
}
