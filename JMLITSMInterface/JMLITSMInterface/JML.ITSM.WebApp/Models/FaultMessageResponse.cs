﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Xml;
using System.Xml.Schema;
using System.Web;

namespace JML.ITSM.WebApp.Models
{
    [DataContract(IsReference = true)]
    [XmlRoot(ElementName = "FaultMessage")]
    public class FaultMessageResponse
    {
        [XmlAttribute]
        public string encoding = "ISO-8859-1";

        public FaultMessageResponse()
        {
            Header = new JMLMessageResponseHeader();
            Response = new JMLMessageResponse();
        }

        [DataMember(Order=0)]
        public JMLMessageResponseHeader Header { get; set; }

        [DataMember(Order=1)]
        public JMLMessageResponse Response { get; set; }
    }
}