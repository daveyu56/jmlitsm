﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace JML.ITSM.WebApp.Models
{
  
    public class JMLCallData
    {
        [XmlElement]
        public string ticketID { get; set; }

        [XmlElement]
        public string extticket { get; set; }

        [XmlElement]
        public string status { get; set; }

        [XmlElement]
        public string priority { get; set; }

        [XmlElement]
        public string severity { get; set; }

        [XmlElement]
        public string callType { get; set; }

        [XmlElement]
        public string source { get; set; }

        [XmlElement]
        public string workgroup { get; set; }

        [XmlElement("additionalAttribute")]
        public List<JMLAdditionalAttribute> additionalAttributes { get; set; }

        [XmlElement]
        public string problemtitle { get; set; }
    }

}
