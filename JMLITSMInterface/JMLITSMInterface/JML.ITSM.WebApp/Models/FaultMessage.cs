﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace JML.ITSM.WebApp.Models
{
    [DataContract(IsReference = true)]
    public class FaultMessage
    {


        [DataMember]
        public JMLMessageHeader Header { get; set; }

        [DataMember]
        public JMLMessageRequest Request { get; set; }
    }
}