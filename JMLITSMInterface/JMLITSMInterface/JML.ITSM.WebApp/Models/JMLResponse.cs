﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace JML.ITSM.WebApp.Models
{
    public class JMLResponse
    {

        private const string XML_HEADER = @"<?xml version=""1.0"" encoding=""UTF-8""?>";
        private const string MESSAGE_HEADER_START = @"<FaultMessage version=""1.1"">";
        private const string MESSAGE_HEADER_END = @"</FaultMessage>";
        
        private const string HEADER_START_TAG = @"<Header>";
        private const string HEADER_END_TAG = @"</Header>";
        private const string RESPONSE_START_TAG = @"<Response>";
        private const string RESPONSE_END_TAG = @"</Response>";
        private const string MESSAGE_TYPE_START_TAG = "<messageType>";
        private const string MESSAGE_TYPE_END_TAG = "</messageType>";
        private const string MESSAGE_ID_START_TAG = "<messageID>";
        private const string MESSAGE_ID_END_TAG = "</messageID>";
        private const string MESSAGE_CRMNAME_START_TAG = "<CRMName>";
        private const string MESSAGE_CRMNAME_END_TAG = "</CRMName>";
        private const string MESSAGE_TICKETID_START_TAG = "<ticketID>";
        private const string MESSAGE_TICKETID_END_TAG = "</ticketID>";
        private const string MESSAGE_EXTTICKET_START_TAG = "<extticket>";
        private const string MESSAGE_EXTTICKET_END_TAG = "</extticket>";
        private const string MESSAGE_MASTERID_START_TAG = "<masterID>";
        private const string MESSAGE_MASTERID_END_TAG = "</masterID>";

        private const string MESSAGE_ERRORCODE_START_TAG = "<errorCode>";
        private const string MESSAGE_ERRORCODE_END_TAG = "</errorCode>";
        private const string MESSAGE_RESMESSAGE_START_TAG = "<resMessage>";
        private const string MESSAGE_RESMESSAGE_END_TAG = "</resMessage>";

     
        


        public string MessageType { get; set; }
        public string MessageId { get; set; }
        public string CRMName { get; set; }
        public string TicketId { get; set; }
        public string ExtTicket { get; set; }
        public string MasterId { get; set; }
        public string ErrorCode { get; set; }
        public string ResMessage { get; set; }

        public string XML 
        {
            get
            {
                return FormatXML();
            }
        }

        private string FormatXML()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(XML_HEADER);
            sb.Append(Environment.NewLine);
            sb.Append(MESSAGE_HEADER_START);
            sb.Append(Environment.NewLine);
            sb.Append(HEADER_START_TAG);
            sb.Append(Environment.NewLine);

            sb.Append(MESSAGE_TYPE_START_TAG);
            sb.Append(MessageType);
            sb.Append(MESSAGE_TYPE_END_TAG);
            sb.Append(Environment.NewLine);

            sb.Append(MESSAGE_ID_START_TAG);
            sb.Append(MessageId);
            sb.Append(MESSAGE_ID_END_TAG);
            sb.Append(Environment.NewLine);

            sb.Append(MESSAGE_CRMNAME_START_TAG);
            sb.Append(CRMName);
            sb.Append(MESSAGE_CRMNAME_END_TAG);
            sb.Append(Environment.NewLine);

            sb.Append(HEADER_END_TAG);
            sb.Append(Environment.NewLine);

            sb.Append(RESPONSE_START_TAG);
            sb.Append(Environment.NewLine);

            sb.Append(MESSAGE_TICKETID_START_TAG);
            sb.Append(TicketId);
            sb.Append(MESSAGE_TICKETID_END_TAG);
            sb.Append(Environment.NewLine);

            sb.Append(MESSAGE_EXTTICKET_START_TAG);
            sb.Append(ExtTicket);
            sb.Append(MESSAGE_EXTTICKET_END_TAG);
            sb.Append(Environment.NewLine);

            sb.Append(MESSAGE_MASTERID_START_TAG);
            sb.Append(MasterId);
            sb.Append(MESSAGE_MASTERID_END_TAG);
            sb.Append(Environment.NewLine);

            sb.Append(MESSAGE_ERRORCODE_START_TAG);
            sb.Append(ErrorCode);
            sb.Append(MESSAGE_ERRORCODE_END_TAG);
            sb.Append(Environment.NewLine);


            sb.Append(MESSAGE_RESMESSAGE_START_TAG);
            sb.Append(ResMessage);
            sb.Append(MESSAGE_RESMESSAGE_END_TAG);
            sb.Append(Environment.NewLine);

            sb.Append(RESPONSE_END_TAG);
            sb.Append(Environment.NewLine);

            sb.Append(MESSAGE_HEADER_END);
          
            return sb.ToString();

        }
    }
}