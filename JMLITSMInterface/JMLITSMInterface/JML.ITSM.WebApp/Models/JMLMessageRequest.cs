﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace JML.ITSM.WebApp.Models
{
    [DataContract(IsReference = true)]
    public class JMLMessageRequest
    {
        [DataMember]
        public JMLCallData calldata {get; set;}

        [DataMember]
        public JMLAttachment attachment { get; set; }

        [DataMember]
        public string problemText { get; set; }

        [DataMember]
        public JMLCustomerData customerdata {get; set;}
    }
}
