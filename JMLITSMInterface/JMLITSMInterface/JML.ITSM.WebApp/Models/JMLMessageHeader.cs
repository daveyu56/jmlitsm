﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace JML.ITSM.WebApp.Models
{

    public class JMLMessageHeader
    {
        public string CRMName { get; set; }
        public string messageType { get; set; }
        public string statuschange { get; set; }
    }
}
