﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Xml;
using JML.ITSM.WebApp.Models;
using JML.ITSM.Domain;
using JML.ITSM.BusinessLogic;
using JML.ITSM.DAL;
using JML.ITSM.Model;
using JML.ITSM.Logging;

namespace JML.ITSM.WebApp.Controllers
{
    public class JMLOldController : ApiController
    {
        IJMLRepository _repository;
        JMLITSMEntities _context;
        JMLDirector _director;

        private readonly string INVALID_JML_REQUEST_RECEIVED = "Invalid JML Request Received";
        private static string ACK_RESPONSE_MESSAGE_TYPE = "ACKNOWLEDGEMENT";

        public JMLOldController(IJMLRepository repository, JMLITSMEntities context, JMLDirector director)
        {
            _context = context;
            _repository = repository;
            _director = director;
        }


        public FaultMessageResponse Get()
        {
            FaultMessageResponse response = new FaultMessageResponse();


            return response;

         
        }

        // POST api/<controller>
       
        public FaultMessageResponse Post([FromBody] FaultMessage faultMessage)
        {

            try
            {
                if (faultMessage != null)
                {
                    ProcessJMLMessageResponse response = ProcessJMLMessage(faultMessage);

                    
                    return SuccessResponse(faultMessage, response);
                }
                else
                {
                    throw new ApplicationException(INVALID_JML_REQUEST_RECEIVED);
                }
            }
            catch (ApplicationException ae)
            {
                return FailedResponse(faultMessage, ae);
            }
            catch (WebException we)
            {
                return FailedResponse(faultMessage, we);
            }
            catch (Exception e)
            {
                return FailedResponse(faultMessage, e);
            }
          
        }

        private ProcessJMLMessageResponse ProcessJMLMessage(FaultMessage faultMessage)
        {
            JMLRequest request = CreateRequestFromMessage(faultMessage);

            return _director.SaveJMLRequest(request);


        }

        private JMLRequest CreateRequestFromMessage(FaultMessage faultMessage)
        {
            JMLRequest request = new JMLRequest();

            request.MessageType = faultMessage.Header.messageType;
            request.TicketId = faultMessage.Request.calldata.ticketID;
            request.ITSMTicketId = string.Empty;
            request.InternalTicketId = faultMessage.Request.calldata.extticket;
            request.MasterId = Guid.NewGuid();
            request.Priority = faultMessage.Request.calldata.priority;
            request.Severity = faultMessage.Request.calldata.severity;
            request.TicketStatus = faultMessage.Request.calldata.status;
            request.CallType = faultMessage.Request.calldata.callType;
            request.Source = faultMessage.Request.calldata.source;
            request.ProblemTitle = faultMessage.Request.calldata.problemtitle;
            request.ProblemText = faultMessage.Request.problemText;
            request.UserLoginId = faultMessage.Request.customerdata.GID;
            request.QueueStatusId = (int)QueueStatusEnum.Unprocessed;
            request.Category = faultMessage.Request.calldata.additionalAttributes[1].Value;
            request.Type = faultMessage.Request.calldata.additionalAttributes[2].Value;
            request.Item = faultMessage.Request.calldata.additionalAttributes[3].Value;
            request.Attachment = faultMessage.Request.attachment.Value;
            request.AttachmentName = faultMessage.Request.attachment.name;
   
            return request;
        }

        private FaultMessageResponse FailedResponse(FaultMessage faultMessage, Exception e)
        {
            FaultMessageResponse response = new FaultMessageResponse();

            Log.LogException("Failed Response Sent", e);

            response.Header.messageType = ACK_RESPONSE_MESSAGE_TYPE;
            response.Header.CRMName = faultMessage != null ? faultMessage.Header.CRMName : string.Empty;
            response.Header.messageID = string.Empty;
            
            response.Response.ticketID = faultMessage != null ? faultMessage.Request.calldata.ticketID : string.Empty;
            response.Response.extticket = string.Empty;
            response.Response.masterID = Guid.NewGuid();
            response.Response.errorCode = "-1";
            response.Response.resMessage = e.Message;

            _director.LogResponse(new JMLResponse(response.Header.messageType,
                                                  response.Header.CRMName,
                                                  response.Header.messageID,
                                                  response.Response.ticketID,
                                                  response.Response.extticket,
                                                  response.Response.masterID,
                                                  response.Response.errorCode,
                                                  response.Response.resMessage));
            

            return response;

        
        }

        private FaultMessageResponse SuccessResponse(FaultMessage faultMessage, ProcessJMLMessageResponse message)
        {
            FaultMessageResponse response = new FaultMessageResponse();

            response.Header.messageType = ACK_RESPONSE_MESSAGE_TYPE;
            response.Header.CRMName = faultMessage != null ? faultMessage.Header.CRMName : string.Empty;
            response.Header.messageID = string.Empty;

            response.Response.ticketID = faultMessage != null ? faultMessage.Request.calldata.ticketID : string.Empty;
            response.Response.extticket = message.InternalTicketId;
            response.Response.masterID = message.MasterId;
            response.Response.errorCode = "0";
            response.Response.resMessage = "OK";

            _director.LogResponse(new JMLResponse(response.Header.messageType,
                                                  response.Header.CRMName,
                                                  response.Header.messageID,
                                                  response.Response.ticketID,
                                                  response.Response.extticket,
                                                  response.Response.masterID,
                                                  response.Response.errorCode,
                                                  response.Response.resMessage));

            return response;
        }

              
    }
    
   
}