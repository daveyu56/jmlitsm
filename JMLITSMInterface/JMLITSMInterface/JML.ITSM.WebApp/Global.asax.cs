﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using System.Xml.Serialization;
using JML.ITSM.WebApp.Models;

namespace JML.ITSM.WebApp
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);

            RouteTable.Routes.RouteExistingFiles = true;

           // WebApiConfig.Register(GlobalConfiguration.Configuration);

           //var xml = GlobalConfiguration.Configuration.Formatters.XmlFormatter;
           //xml.SetSerializer<JMLMessage>(new XmlSerializer(typeof(JMLMessage)));
           //xml.Indent = true;
           //xml.UseXmlSerializer = true;

            var xml = GlobalConfiguration.Configuration.Formatters.XmlFormatter;
            xml.UseXmlSerializer = true;
            xml.Indent = true;

        }
    }
}
