﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdminHome.aspx.cs" Inherits="JML.ITSM.WebUI.Admin.AdminHome" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <section class="featured">
        <div class="content-wrapper">
            <hgroup class="title">
                
            </hgroup>
            <p>
                JML-ITSM Interface Admin Home Page
            </p>
        </div>
    </section>
</asp:Content>
<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
   
    <ol class="round">

        
        <li class="one">
            <h5><a href="/Admin/OpCats">Operational Categories</a></h5>
            Administer Operational Categories.
        
        </li>
     
        <li class="two">
            <h5><a href="/Admin/Impact">Impact</a></h5>
            Administer Impact mappings.
       
        </li>

        <li class="three">
            <h5><a href="/Admin/Urgency">Urgency</a></h5>
            Administer Urgency mappings.
        </li>

          <li class="four">
            <h5><a href="/Admin/ConfigurationAdmin">Configuration Settings</a></h5>
            Administer Configuration Settings.
        
        </li>
        
    </ol>

</asp:Content>
