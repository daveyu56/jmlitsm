﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JML.ITSM.BusinessLogic;
using JML.ITSM.Domain;
using Ninject;
using Ninject.Web;



namespace JML.ITSM.WebUI.Admin
{
    public partial class Urgency : Ninject.Web.PageBase
    {

        [Inject]
        public AdminDirector Director { get; set; }

      
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void UrgencyGridView_DataBound(object sender, EventArgs e)
        {
            //GridView gridView = (GridView)sender;

            //if (gridView.HeaderRow != null && gridView.HeaderRow.Cells.Count > 0)
            //{
            //    gridView.HeaderRow.Cells[1].Visible = false;
            //}

            //foreach (GridViewRow row in UrgencyGridView.Rows)
            //{
            //    row.Cells[1].Visible = false;
            //}
        }

        protected void UrgencyGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = UrgencyGridView.EditIndex;
            GridViewRow row = UrgencyGridView.Rows[index];

            TextBox id = (TextBox)row.FindControl("tbId");
            TextBox JMLUrgency = (TextBox)row.FindControl("tbJMLUrgency");
            TextBox ITSMUrgency = (TextBox)row.FindControl("tbITSMUrgency");

            e.NewValues["Id"] = id.Text.Trim();
            e.NewValues["JMLUrgency"] = JMLUrgency.Text.Trim();
            e.NewValues["ITSMUrgency"] = ITSMUrgency.Text.Trim();
        }

        protected void UrgencyGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Insert")
            {
                TextBox tbJMLUrgency = UrgencyGridView.FooterRow.FindControl("tbJMLUrgency") as TextBox;
                TextBox tbITSMUrgency = UrgencyGridView.FooterRow.FindControl("tbITSMUrgency") as TextBox;

                UrgencyData urgency = new UrgencyData();
                urgency.JMLUrgency = tbJMLUrgency.Text.Trim();
                urgency.ITSMUrgency = tbITSMUrgency.Text.Trim();

                Director.InsertUrgency(urgency);

            }

            if (e.CommandName == "Delete")
            {
                LinkButton lb = (LinkButton)e.CommandSource;
                int index = Convert.ToInt32(lb.CommandArgument);

                GridViewRow row = UrgencyGridView.Rows[index];

                TextBox hdnId = (TextBox)row.FindControl("tbId");

                UrgencyData urgency = new UrgencyData();
                urgency.Id = Convert.ToInt32(hdnId.Text.Trim());
                Director.DeleteUrgency(urgency);


            }
        }

       

          
    }
}