﻿<%@ Page Title="JMLITSMReport" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="OpCats.aspx.cs" Inherits="JML.ITSM.WebUI.Admin.OpCats" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
      
    </hgroup>

    

    <section class="contact">
        <header>
             <p>
                Operational Categories Mapping Administration
            </p>
        </header>

        <p>
                <asp:GridView ID="OpCatsGridView" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="odsOpsCats" ShowFooter="True" OnRowCommand="OpCatsGridView_RowCommand"
                              BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical" OnRowUpdating="OpCatsGridView_RowUpdating"
                    >
                    <Columns>

                     

                       <asp:TemplateField ShowHeader="False">
                        <EditItemTemplate>
                            <asp:ImageButton ID="LinkButton1" Height="15px" Width="15px" ImageUrl="~/Images/tick.png" ToolTip="Save" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:ImageButton>
                            &nbsp;
                            <asp:ImageButton ID="LinkButton2" Height="15px" Width="15px" ImageUrl="~/Images/cancel.png" ToolTip="Cancel" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:ImageButton>
                        </EditItemTemplate>

                        <AlternatingItemTemplate>
                            <asp:ImageButton ID="LinkButton1" ToolTip="Edit Impact" BackColor="white" BorderColor="white" Height="15px" Width="15px" ImageUrl="~/Images/edit.png" runat="server" CausesValidation="False" CommandName="Edit"></asp:ImageButton>
                            &nbsp;
                            <asp:ImageButton ID="LinkButton3" ToolTip="Delete Impact" BackColor="white" BorderColor="white" Height="15px" Width="15px" ImageUrl="~/Images/trashcan.png" runat="server" CausesValidation="False" CommandName="Delete" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" ></asp:ImageButton>
                        </AlternatingItemTemplate>

                        <ItemTemplate>
                            <asp:ImageButton ID="LinkButton1" BackColor="#F7F7DE" BorderColor="#F7F7DE" Height="15px" Width="15px" ImageUrl="~/Images/edit.png" ToolTip="Edit Impact" runat="server" CausesValidation="False" CommandName="Edit" ></asp:ImageButton>
                            &nbsp;
                            <asp:ImageButton ID="LinkButton3" BackColor="#F7F7DE" BorderColor="#F7F7DE" Height="15px" Width="15px" ImageUrl="~/Images/trashcan.png" ToolTip="Delete Impact" runat="server" CausesValidation="False" CommandName="Delete" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" ></asp:ImageButton>
                        </ItemTemplate>

                        <FooterTemplate>
                            <asp:ImageButton Tooltip="Insert Impact" Height="15px" Width="15px" ImageUrl="~/Images/insert.png" CommandName="Insert" CausesValidation="true" runat="server" ID="btInsert" />&nbsp;
                            <asp:ImageButton Tooltip="Cancel" Height="15px" Width="15px" ImageUrl="~/Images/cancel.png" CommandName="Cancel" CausesValidation="false" runat="server" ID="btCancel" />
                        </FooterTemplate>

                    </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="Id" SortExpression="Id">
                            <%-- <EditItemTemplate>
                                <asp:TextBox ID="tbId" runat="server" Text='<%# Bind("Id") %>'></asp:TextBox>
                            </EditItemTemplate>--%>
                            <ItemTemplate>
                                <asp:Label ID="tbId" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                            </ItemTemplate>
                               <FooterTemplate>
                                <asp:TextBox ID="tbId" runat="server" BorderColor="#CCCC99"  BackColor ="#CCCC99" ForeColor="#CCCC99" ReadOnly="true" Height="0px" Width="0px" Text='<%# Bind("Id_new") %>'></asp:TextBox>
                               </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Category" SortExpression="Category">
                            <EditItemTemplate>
                                <asp:TextBox ID="tbCategory" runat="server" Text='<%# Bind("Category") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="tbCategory" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                            </ItemTemplate>
                             <FooterTemplate>
                                <asp:TextBox ID="tbCategory" runat="server" Height="15px" Width="120px" Text='<%# Bind("Category_new") %>'></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Type" SortExpression="Type">
                            <EditItemTemplate>
                                <asp:TextBox ID="tbType" runat="server" Text='<%# Bind("Type") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="tbType" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                            </ItemTemplate>
                             <FooterTemplate>
                                <asp:TextBox ID="tbType" runat="server" Height="15px" Width="120px" Text='<%# Bind("Type_new") %>'></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Item" SortExpression="Item">
                            <EditItemTemplate>
                                <asp:TextBox ID="tbItem" runat="server" Text='<%# Bind("Item") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="tbItem" runat="server" Text='<%# Bind("Item") %>'></asp:Label>
                            </ItemTemplate>
                             <FooterTemplate>
                                <asp:TextBox ID="tbItem" runat="server" Height="15px" Width="120px" Text='<%# Bind("Item_new") %>'></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Title" SortExpression="Title">
                            <EditItemTemplate>
                                <asp:TextBox ID="tbTitle" runat="server" Text='<%# Bind("Title") %>'></asp:TextBox>
                            </EditItemTemplate>
                            <ItemTemplate>
                                <asp:Label ID="tbTitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                            </ItemTemplate>
                             <FooterTemplate>
                                <asp:TextBox ID="tbTitle" runat="server" Height="15px" Width="270px" Text='<%# Bind("Title_new") %>'></asp:TextBox>
                            </FooterTemplate>
                        </asp:TemplateField>

                    </Columns>

                    <RowStyle BackColor="#F7F7DE" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="#CCCC99" />
                    <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />            
                    <AlternatingRowStyle BackColor="White" />

                   

                </asp:GridView>

                <asp:ObjectDataSource ID="odsOpsCats" 
                    runat="server" DataObjectTypeName="JML.ITSM.Domain.OpCatsData"
                    DeleteMethod="DeleteOpCat" 
                    InsertMethod="InsertOpCat" 
                    SelectMethod="GetOpCatsData" 
                    TypeName="JML.ITSM.BusinessLogic.AdminDirector" 
                    UpdateMethod="UpdateOpCat">
                   
                </asp:ObjectDataSource>
        </p>

       
      
    </section>

    


   
</asp:Content>