﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JML.ITSM.BusinessLogic;
using JML.ITSM.Domain;
using Ninject;
using Ninject.Web;



namespace JML.ITSM.WebUI.Admin
{
    public partial class ConfigurationAdmin : Page
    {
      
        [Inject]
        public AdminDirector Director { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = "JML ITSM Interface";

            if (!this.IsPostBack)
            {
               

               
            }
        }

        protected void ConfigGridView_DataBound(object sender, EventArgs e)
        {
            //GridView gridView = (GridView)sender;

            //if (gridView.HeaderRow != null && gridView.HeaderRow.Cells.Count > 0)
            //{
            //    gridView.HeaderRow.Cells[1].Visible = false;
            //}

            //foreach (GridViewRow row in ConfigGridView.Rows)
            //{
            //    row.Cells[1].Visible = false;
            //}
        }

        protected void ConfigGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Insert")
            {
                TextBox tbSettingName = ConfigGridView.FooterRow.FindControl("tbSettingName") as TextBox;
                TextBox tbSettingValue = ConfigGridView.FooterRow.FindControl("tbSettingValue") as TextBox;

                ConfigurationData config = new ConfigurationData();
                config.SettingName = tbSettingName.Text.Trim();
                config.SettingValue = tbSettingValue.Text.Trim();


                Director.InsertConfiguraton(config);

            }

           
        }

        protected void ConfigGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = ConfigGridView.EditIndex;
            GridViewRow row = ConfigGridView.Rows[index];

            TextBox id = (TextBox)row.FindControl("tbId");
            TextBox SettingName = (TextBox)row.FindControl("tbSettingName");
            TextBox SettingValue = (TextBox)row.FindControl("tbSettingValue");

            e.NewValues["Id"] = id.Text.Trim();
            e.NewValues["SettingName"] = SettingName.Text.Trim();
            e.NewValues["SettingValue"] = SettingValue.Text.Trim();
        }

          
    }
}