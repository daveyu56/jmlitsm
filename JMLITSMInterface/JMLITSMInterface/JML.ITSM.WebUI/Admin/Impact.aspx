﻿<%@ Page Title="JMLITSMReport" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Impact.aspx.cs" Inherits="JML.ITSM.WebUI.Admin.Impact" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
       
    </hgroup>

    <section class="contact">
        <header>
              <p>
                Impact Mapping Administration
            </p>
        </header>

        <p>
            <asp:GridView ID="ImpactGridView" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1"
                  BackColor="White" BorderColor="#DEDFDE" 
                 BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical" OnDataBound="ImpactGridView_DataBound" OnRowCommand="ImpactGridView_RowCommand" ShowFooter="True" OnRowUpdating="ImpactGridView_RowUpdating"  >
                <Columns>
               
                  

                    <asp:TemplateField ShowHeader="False">
                        <EditItemTemplate>
                            <asp:ImageButton ID="LinkButton1" Height="15px" Width="15px" ImageUrl="~/Images/tick.png" ToolTip="Save" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:ImageButton>
                            &nbsp;
                            <asp:ImageButton ID="LinkButton2" Height="15px" Width="15px" ImageUrl="~/Images/cancel.png" ToolTip="Cancel" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:ImageButton>
                        </EditItemTemplate>

                        <AlternatingItemTemplate>
                            <asp:ImageButton ID="LinkButton1" ToolTip="Edit Impact" BackColor="white" BorderColor="white" Height="15px" Width="15px" ImageUrl="~/Images/edit.png" runat="server" CausesValidation="False" CommandName="Edit"></asp:ImageButton>
                            &nbsp;
                            <asp:ImageButton ID="LinkButton3" ToolTip="Delete Impact" BackColor="white" BorderColor="white" Height="15px" Width="15px" ImageUrl="~/Images/trashcan.png" runat="server" CausesValidation="False" CommandName="Delete" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" ></asp:ImageButton>
                        </AlternatingItemTemplate>

                        <ItemTemplate>
                            <asp:ImageButton ID="LinkButton1" BackColor="#F7F7DE" BorderColor="#F7F7DE" Height="15px" Width="15px" ImageUrl="~/Images/edit.png" ToolTip="Edit Impact" runat="server" CausesValidation="False" CommandName="Edit" ></asp:ImageButton>
                            &nbsp;
                            <asp:ImageButton ID="LinkButton3" BackColor="#F7F7DE" BorderColor="#F7F7DE" Height="15px" Width="15px" ImageUrl="~/Images/trashcan.png" ToolTip="Delete Impact" runat="server" CausesValidation="False" CommandName="Delete" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>" ></asp:ImageButton>
                        </ItemTemplate>

                        <FooterTemplate>
                            <asp:ImageButton BackColor="#CCCC99" BorderColor="#CCCC99" Tooltip="Insert Impact" Height="20px" Width="20px" ImageUrl="~/Images/insert.png" CommandName="Insert" CausesValidation="true" runat="server" ID="btInsert" />&nbsp;
                            <asp:ImageButton BackColor="#CCCC99" BorderColor="#CCCC99" Tooltip="Cancel" Height="15px" Width="15px" ImageUrl="~/Images/cancel.png" CommandName="Cancel" CausesValidation="false" runat="server" ID="btCancel" />
                        </FooterTemplate>

                    </asp:TemplateField>

                      <asp:TemplateField  HeaderText="Id" SortExpression="Id">
                       <%-- <EditItemTemplate>
                            <asp:TextBox ID="tbId" runat="server" Text='<%# Bind("Id") %>'></asp:TextBox>
                        </EditItemTemplate>--%>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                        </AlternatingItemTemplate>

                        <FooterTemplate>
                            <asp:TextBox ID="tbId" runat="server" BorderColor="#CCCC99"  BackColor ="#CCCC99" ForeColor="#CCCC99" ReadOnly="true" Height="0px" Width="0px" Text='<%# Bind("Id_new") %>'></asp:TextBox>
                        </FooterTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="JML Impact" SortExpression="JMLImpact">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbJMLImpact" runat="server" Text='<%# Bind("JMLImpact") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("JMLImpact") %>'></asp:Label>
                        </ItemTemplate>
                        <AlternatingItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("JMLImpact") %>'></asp:Label>
                        </AlternatingItemTemplate>

                        <FooterTemplate>
                            <asp:TextBox ID="tbJMLImpact" runat="server" Height="15px" Width="120px" Text='<%# Bind("JMLImpact_new") %>'></asp:TextBox>
                        </FooterTemplate>


                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="ITSM Impact" SortExpression="ITSMImpact">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbITSMImpact" runat="server" Text='<%# Bind("ITSMImpact") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("ITSMImpact") %>'></asp:Label>
                        </ItemTemplate>

                        <AlternatingItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Bind("ITSMImpact") %>'></asp:Label>
                        </AlternatingItemTemplate>

                        <FooterTemplate>
                            <asp:TextBox ID="tbITSMImpact" runat="server" Height="15px" Width="120px" Text='<%# Bind("ITSMImpact_new") %>'></asp:TextBox>
                        </FooterTemplate>


                    </asp:TemplateField>

                </Columns>

                  <RowStyle BackColor="#F7F7DE" />
                 <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                  <FooterStyle BackColor="#CCCC99" />
                  <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />            
                 <AlternatingRowStyle BackColor="White" />

            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DataObjectTypeName="JML.ITSM.Domain.ImpactData" SelectMethod="GetImpactData" TypeName="JML.ITSM.BusinessLogic.AdminDirector" UpdateMethod="UpdateImpact" DeleteMethod="DeleteImpact"></asp:ObjectDataSource>
        </p>

       
      
    </section>

    


   
</asp:Content>