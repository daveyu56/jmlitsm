﻿<%@ Page Title="JMLITSMReport" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConfigurationAdmin.aspx.cs" Inherits="JML.ITSM.WebUI.Admin.ConfigurationAdmin" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
      
    </hgroup>

    

    <section class="contact">
        <header>
             <p>
                Configuration Administration
            </p>
        </header>

        <p>
            <asp:GridView ID="ConfigGridView" runat="server" AllowPaging="True" ShowFooter="true" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1"
                  BackColor="White" BorderColor="#DEDFDE" 
                 BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical" OnDataBound="ConfigGridView_DataBound" OnRowCommand="ConfigGridView_RowCommand" OnRowUpdating="ConfigGridView_RowUpdating" >
                <Columns>
                   
                 
                    <asp:TemplateField ShowHeader="False">
                        <EditItemTemplate>
                            <asp:ImageButton ID="LinkButton1" Height="15px" Width="15px" ImageUrl="~/Images/tick.png" ToolTip="Save" runat="server" CausesValidation="True" CommandName="Update" Text="Update"></asp:ImageButton>
                            &nbsp;
                            <asp:ImageButton ID="LinkButton2" Height="15px" Width="15px" ImageUrl="~/Images/cancel.png" ToolTip="Cancel" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel"></asp:ImageButton>
                        </EditItemTemplate>

                        <AlternatingItemTemplate>
                            <asp:ImageButton ID="LinkButton1" ToolTip="Edit Impact" BackColor="white" BorderColor="white" Height="15px" Width="15px" ImageUrl="~/Images/edit.png" runat="server" CausesValidation="False" CommandName="Edit"></asp:ImageButton>
                           
                        </AlternatingItemTemplate>

                        <ItemTemplate>
                            <asp:ImageButton ID="LinkButton1" BackColor="#F7F7DE" BorderColor="#F7F7DE" Height="15px" Width="15px" ImageUrl="~/Images/edit.png" ToolTip="Edit Impact" runat="server" CausesValidation="False" CommandName="Edit" ></asp:ImageButton>
                          
                        </ItemTemplate>

                        <FooterTemplate>
                            <asp:ImageButton Tooltip="Insert Impact" Height="15px" Width="15px" ImageUrl="~/Images/insert.png" CommandName="Insert" CausesValidation="true" runat="server" ID="btInsert" />&nbsp;
                            <asp:ImageButton Tooltip="Cancel" Height="15px" Width="15px" ImageUrl="~/Images/cancel.png" CommandName="Cancel" CausesValidation="false" runat="server" ID="btCancel" />
                        </FooterTemplate>

                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Id" SortExpression="Id">
                      <%-- <EditItemTemplate>
                            <asp:TextBox ID="tbId" runat="server" Text='<%# Bind("Id") %>'></asp:TextBox>
                        </EditItemTemplate>--%>
                        <ItemTemplate>
                            <asp:Label ID="tbId" runat="server" Text='<%# Bind("Id") %>'></asp:Label>
                        </ItemTemplate>
                         <FooterTemplate>
                            <asp:TextBox ID="tbId" runat="server" BorderColor="#CCCC99"  BackColor ="#CCCC99" ForeColor="#CCCC99" ReadOnly="true" Height="0px" Width="0px" Text='<%# Bind("Id_new") %>'></asp:TextBox>
                        </FooterTemplate>
                         
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="SettingName" SortExpression="SettingName">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbSettingName" runat="server" Text='<%# Bind("SettingName") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="tbSettingName" runat="server" Text='<%# Bind("SettingName") %>'></asp:Label>
                        </ItemTemplate>
                         <FooterTemplate>
                            <asp:TextBox ID="tbSettingName" runat="server" Height="15px" Width="120px" Text='<%# Bind("SettingName_new") %>'></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="SettingValue" SortExpression="SettingValue">
                        <EditItemTemplate>
                            <asp:TextBox ID="tbSettingValue" runat="server" Text='<%# Bind("SettingValue") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="tbSettingValue" runat="server" Text='<%# Bind("SettingValue") %>'></asp:Label>
                        </ItemTemplate>
                         <FooterTemplate>
                            <asp:TextBox ID="tbSettingValue" runat="server" Height="15px" Width="120px" Text='<%# Bind("SettingValue_new") %>'></asp:TextBox>
                        </FooterTemplate>
                    </asp:TemplateField>

                </Columns>
                     <RowStyle BackColor="#F7F7DE" />
                 <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                  <FooterStyle BackColor="#CCCC99" />
                  <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />            
                 <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DataObjectTypeName="JML.ITSM.Domain.ConfigurationData" DeleteMethod="DeleteConfiguraton" InsertMethod="InsertConfiguraton" SelectMethod="GetConfigurationData" TypeName="JML.ITSM.BusinessLogic.AdminDirector" UpdateMethod="UpdateConfiguratonData"></asp:ObjectDataSource>
        </p>

       
      
    </section>

    


   
</asp:Content>