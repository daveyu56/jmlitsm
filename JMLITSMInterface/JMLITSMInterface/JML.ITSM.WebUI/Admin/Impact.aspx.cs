﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JML.ITSM.BusinessLogic;
using JML.ITSM.Domain;
using Ninject;
using Ninject.Web;



namespace JML.ITSM.WebUI.Admin
{
    public partial class Impact : Ninject.Web.PageBase
    {
       

        [Inject]
        public AdminDirector Director { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = "JML ITSM Interface";

            if (!this.IsPostBack)
            {
               

               
            }
        }

        protected void ImpactGridView_DataBound(object sender, EventArgs e)
        {
            //GridView gridView = (GridView)sender;

            //if (gridView.HeaderRow != null && gridView.HeaderRow.Cells.Count > 0)
            //{
            //    gridView.HeaderRow.Cells[1].Visible = false;
            //}

            //foreach (GridViewRow row in ImpactGridView.Rows)
            //{
            //    row.Cells[1].Visible = false;

            //    if (row.RowType == DataControlRowType.Footer)
            //    {
            //        //ImpactGridView.FooterRow.Cells.RemoveAt(1);
            //    }
            //}
        }

        protected void ImpactGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Insert")
            {
                TextBox tbJMLImpact = ImpactGridView.FooterRow.FindControl("tbJMLImpact") as TextBox;
                TextBox tbITSMImpact = ImpactGridView.FooterRow.FindControl("tbITSMImpact") as TextBox;
             
                ImpactData impact = new ImpactData();
                impact.JMLImpact = tbJMLImpact.Text.Trim();
                impact.ITSMImpact = tbITSMImpact.Text.Trim();

                Director.InsertImpact(impact);

            }

            if (e.CommandName == "Delete")
            {
                LinkButton lb = (LinkButton)e.CommandSource;
                int index = Convert.ToInt32(lb.CommandArgument);

                GridViewRow row = ImpactGridView.Rows[index];

                TextBox hdnId = (TextBox)row.FindControl("tbId");

                ImpactData impact = new ImpactData();
                impact.Id = Convert.ToInt32(hdnId.Text.Trim());
                Director.DeleteImpact(impact);


            }

            
        }

        protected void ImpactGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = ImpactGridView.EditIndex;
            GridViewRow row = ImpactGridView.Rows[index];

            TextBox id = (TextBox)row.FindControl("tbId");
            TextBox JMLImpact = (TextBox)row.FindControl("tbJMLImpact");
            TextBox ITSMImpact = (TextBox)row.FindControl("tbITSMImpact");

            e.NewValues["Id"] = id.Text.Trim();
            e.NewValues["JMLImpact"] = JMLImpact.Text.Trim();
            e.NewValues["ITSMImpact"] = ITSMImpact.Text.Trim();
        }

     

       

          
    }
}