﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JML.ITSM.BusinessLogic;
using JML.ITSM.Domain;
using Ninject;
using Ninject.Web;



namespace JML.ITSM.WebUI.Admin
{
    public partial class OpCats : Ninject.Web.PageBase
    {

        [Inject]
        public AdminDirector Director { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
         
        }

        protected void OpCatsGridView_DataBound(object sender, EventArgs e)
        {
            //GridView gridView = (GridView)sender;

            //if (gridView.HeaderRow != null && gridView.HeaderRow.Cells.Count > 0)
            //{
            //    gridView.HeaderRow.Cells[1].Visible = false;
            //}

            //foreach (GridViewRow row in OpCatsGridView.Rows)
            //{
            //    row.Cells[1].Visible = false;
            //}
        }

        protected void OpCatsGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
             if (e.CommandName == "Insert")
             {
                 TextBox tbCategory = OpCatsGridView.FooterRow.FindControl("tbCategory") as TextBox;
                 TextBox tbType = OpCatsGridView.FooterRow.FindControl("tbType") as TextBox;
                 TextBox tbItem = OpCatsGridView.FooterRow.FindControl("tbItem") as TextBox;
                 TextBox tbTitle = OpCatsGridView.FooterRow.FindControl("tbTitle") as TextBox;

                 OpCatsData opCat = new OpCatsData();
                 opCat.Category = tbCategory.Text.Trim();
                 opCat.Type = tbType.Text.Trim();
                 opCat.Item = tbItem.Text.Trim();
                 opCat.Title = tbTitle.Text.Trim();
                 Director.InsertOpCat(opCat);
         
             }

             if (e.CommandName == "Delete")
             {
                 LinkButton lb = (LinkButton)e.CommandSource;
                 int index = Convert.ToInt32(lb.CommandArgument);

                 GridViewRow row = OpCatsGridView.Rows[index];

                 TextBox hdnId = (TextBox)row.FindControl("tbId");

                 OpCatsData opCat = new OpCatsData();
                 opCat.Id = Convert.ToInt32(hdnId.Text.Trim());
                 Director.DeleteOpCat(opCat);
             }
        }

        protected void OpCatsGridView_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            int index = OpCatsGridView.EditIndex;
            GridViewRow row = OpCatsGridView.Rows[index];

            TextBox id = (TextBox)row.FindControl("tbId");
            TextBox tbCategory = row.FindControl("tbCategory") as TextBox;
            TextBox tbType = row.FindControl("tbType") as TextBox;
            TextBox tbItem = row.FindControl("tbItem") as TextBox;
            TextBox tbTitle = row.FindControl("tbTitle") as TextBox;

            e.NewValues["Id"] = id.Text.Trim();
            e.NewValues["Category"] = tbCategory.Text.Trim();
            e.NewValues["Type"] = tbType.Text.Trim();
            e.NewValues["Item"] = tbItem.Text.Trim();
            e.NewValues["Title"] = tbTitle.Text.Trim();
        }

          
    }
}