﻿<%@ Page Title="JMLITSMReport" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JMLITSMReport.aspx.cs" Inherits="JML.ITSM.WebUI.Reporting.JMLITSMReport" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h1><%: Title %>.</h1>
        <h2> Reporting</h2>
    </hgroup>

    <section class="contact">
        <header>
            
        </header>

      
            <div style="float: left;">
                DirX Generated JML-ITSM Interface Tickets - 

                <asp:DropDownList ID="monthDropDown" runat="server" AutoPostBack="True" OnSelectedIndexChanged="monthDropDown_SelectedIndexChanged">
                    <asp:ListItem Value="1">January</asp:ListItem>
                    <asp:ListItem Value="2">February</asp:ListItem>
                    <asp:ListItem Value="3">March</asp:ListItem>
                    <asp:ListItem Value="4">April</asp:ListItem>
                    <asp:ListItem Value="5">May</asp:ListItem>
                    <asp:ListItem Value="6">June</asp:ListItem>
                    <asp:ListItem Value="7">July</asp:ListItem>
                    <asp:ListItem Value="8">August</asp:ListItem>
                    <asp:ListItem Value="9">September</asp:ListItem>
                    <asp:ListItem Value="10">October</asp:ListItem>
                    <asp:ListItem Value="11">November</asp:ListItem>
                    <asp:ListItem Value="12">December</asp:ListItem>

                    
                </asp:DropDownList>

                 <asp:DropDownList ID="yearDropDown" runat="server" AutoPostBack="True" DataSourceID="ObjectDataSource2" OnSelectedIndexChanged="yearDropDown_SelectedIndexChanged">
                 </asp:DropDownList>
                 </div>

                
                   <div style="float: right;"><asp:Button ID="exportToExcel" text="Export to Excel" runat="server" OnClick="exportToExcel_Click"/></div>
               
           
          

        <p>
            <asp:ListView ID="JMLITSMReportListView" runat="server" DataSourceID="ObjectDataSource3">
                <AlternatingItemTemplate>
                    <tr style="background-color: #FFFFFF;color: #284775;">
                        <td>
                            <asp:Label ID="JMLReferenceLabel" runat="server" Text='<%# Eval("JMLReference") %>' />
                        </td>
                        <td>
                            <asp:Label ID="InternalTicketIdLabel" runat="server" Text='<%# Eval("InternalTicketId") %>' />
                        </td>
                        <td>
                            <asp:Label ID="DateAddedLabel" runat="server" Text='<%# Eval("DateAdded") %>' />
                        </td>
                        <td>
                            <asp:Label ID="CategoryLabel" runat="server" Text='<%# Eval("Category") %>' />
                        </td>
                        <td>
                            <asp:Label ID="TypeLabel" runat="server" Text='<%# Eval("Type") %>' />
                        </td>
                        <td>
                            <asp:Label ID="ItemLabel" runat="server" Text='<%# Eval("Item") %>' />
                        </td>
                        <td>
                            <asp:Label ID="OpCatLabel" runat="server" Text='<%# Eval("OpCat") %>' />
                        </td>
                        <td>
                            <asp:Label ID="SummaryLabel" runat="server" Text='<%# Eval("Summary") %>' />
                        </td>
                    </tr>
                </AlternatingItemTemplate>
                <EditItemTemplate>
                    <tr style="background-color: #999999;">
                        <td>
                            <asp:Button ID="UpdateButton" runat="server" CommandName="Update" Text="Update" />
                            <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Cancel" />
                        </td>
                        <td>
                            <asp:TextBox ID="JMLReferenceTextBox" runat="server" Text='<%# Bind("JMLReference") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="InternalTicketIdTextBox" runat="server" Text='<%# Bind("InternalTicketId") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="DateAddedTextBox" runat="server" Text='<%# Bind("DateAdded") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="CategoryTextBox" runat="server" Text='<%# Bind("Category") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="TypeTextBox" runat="server" Text='<%# Bind("Type") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="ItemTextBox" runat="server" Text='<%# Bind("Item") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="OpCatTextBox" runat="server" Text='<%# Bind("OpCat") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="SummaryTextBox" runat="server" Text='<%# Bind("Summary") %>' />
                        </td>
                    </tr>
                </EditItemTemplate>
             
                <InsertItemTemplate>
                    <tr style="">
                        <td>
                            <asp:Button ID="InsertButton" runat="server" CommandName="Insert" Text="Insert" />
                            <asp:Button ID="CancelButton" runat="server" CommandName="Cancel" Text="Clear" />
                        </td>
                        <td>
                            <asp:TextBox ID="JMLReferenceTextBox" runat="server" Text='<%# Bind("JMLReference") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="InternalTicketIdTextBox" runat="server" Text='<%# Bind("InternalTicketId") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="DateAddedTextBox" runat="server" Text='<%# Bind("DateAdded") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="CategoryTextBox" runat="server" Text='<%# Bind("Category") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="TypeTextBox" runat="server" Text='<%# Bind("Type") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="ItemTextBox" runat="server" Text='<%# Bind("Item") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="OpCatTextBox" runat="server" Text='<%# Bind("OpCat") %>' />
                        </td>
                        <td>
                            <asp:TextBox ID="SummaryTextBox" runat="server" Text='<%# Bind("Summary") %>' />
                        </td>
                    </tr>
                </InsertItemTemplate>
                <ItemTemplate>
                    <tr style="background-color: #E0FFFF;color: #333333;">
                        <td>
                            <asp:Label ID="JMLReferenceLabel" runat="server" Text='<%# Eval("JMLReference") %>' />
                        </td>
                        <td>
                            <asp:Label ID="InternalTicketIdLabel" runat="server" Text='<%# Eval("InternalTicketId") %>' />
                        </td>
                        <td>
                            <asp:Label ID="DateAddedLabel" runat="server" Text='<%# Eval("DateAdded") %>' />
                        </td>
                        <td>
                            <asp:Label ID="CategoryLabel" runat="server" Text='<%# Eval("Category") %>' />
                        </td>
                        <td>
                            <asp:Label ID="TypeLabel" runat="server" Text='<%# Eval("Type") %>' />
                        </td>
                        <td>
                            <asp:Label ID="ItemLabel" runat="server" Text='<%# Eval("Item") %>' />
                        </td>
                        <td>
                            <asp:Label ID="OpCatLabel" runat="server" Text='<%# Eval("OpCat") %>' />
                        </td>
                        <td>
                            <asp:Label ID="SummaryLabel" runat="server" Text='<%# Eval("Summary") %>' />
                        </td>
                    </tr>
                </ItemTemplate>
                <LayoutTemplate>
                    <table runat="server">
                        <tr runat="server">
                            <td runat="server">
                                <table id="itemPlaceholderContainer" runat="server" border="1" style="background-color: #FFFFFF;border-collapse: collapse;border-color: #999999;border-style:none;border-width:1px;font-family: Verdana, Arial, Helvetica, sans-serif;">
                                    <tr runat="server" style="background-color: #E0FFFF;color: #333333;">
                                        <th runat="server">JMLReference</th>
                                        <th runat="server">InternalTicketId</th>
                                        <th runat="server">DateAdded</th>
                                        <th runat="server">Category</th>
                                        <th runat="server">Type</th>
                                        <th runat="server">Item</th>
                                        <th runat="server">OpCat</th>
                                        <th runat="server">Summary</th>
                                    </tr>
                                    <tr id="itemPlaceholder" runat="server">
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr runat="server">
                            <td runat="server" style="text-align: center;background-color: #5D7B9D;font-family: Verdana, Arial, Helvetica, sans-serif;color: #FFFFFF">
                                <asp:DataPager ID="DataPager2" runat="server">
                                    <Fields>
                                        <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" />
                                    </Fields>
                                </asp:DataPager>
                            </td>
                        </tr>
                    </table>
                </LayoutTemplate>
                <SelectedItemTemplate>
                    <tr style="background-color: #E2DED6;font-weight: bold;color: #333333;">
                        <td>
                            <asp:Label ID="JMLReferenceLabel" runat="server" Text='<%# Eval("JMLReference") %>' />
                        </td>
                        <td>
                            <asp:Label ID="InternalTicketIdLabel" runat="server" Text='<%# Eval("InternalTicketId") %>' />
                        </td>
                        <td>
                            <asp:Label ID="DateAddedLabel" runat="server" Text='<%# Eval("DateAdded") %>' />
                        </td>
                        <td>
                            <asp:Label ID="CategoryLabel" runat="server" Text='<%# Eval("Category") %>' />
                        </td>
                        <td>
                            <asp:Label ID="TypeLabel" runat="server" Text='<%# Eval("Type") %>' />
                        </td>
                        <td>
                            <asp:Label ID="ItemLabel" runat="server" Text='<%# Eval("Item") %>' />
                        </td>
                        <td>
                            <asp:Label ID="OpCatLabel" runat="server" Text='<%# Eval("OpCat") %>' />
                        </td>
                        <td>
                            <asp:Label ID="SummaryLabel" runat="server" Text='<%# Eval("Summary") %>' />
                        </td>
                    </tr>
                </SelectedItemTemplate>
            </asp:ListView>

            

          
            <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" SelectMethod="GetReportingData" TypeName="JML.ITSM.BusinessLogic.ReportingDirector">
                <SelectParameters>
                    <asp:ControlParameter ControlID="monthDropDown" DefaultValue="6" Name="month" PropertyName="SelectedValue" Type="String" />
                    <asp:ControlParameter ControlID="yearDropDown" DefaultValue="2014" Name="year" PropertyName="SelectedValue" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>

            

          
        </p>

       
      
    </section>

      <asp:ObjectDataSource ID="ObjectDataSource2"
                     runat="server" 
                     SelectMethod="GetReportingYears"
                     TypeName="JML.ITSM.DAL.Datasources.JMLITSMReportingDS">

      </asp:ObjectDataSource>


   
</asp:Content>