﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JML.ITSM.BusinessLogic;
using JML.ITSM.Domain;
using Ninject;
using Ninject.Web;
using Microsoft.Office.Interop.Excel;


namespace JML.ITSM.WebUI.Reporting
{
    public partial class JMLITSMReport : Ninject.Web.PageBase
    {
        private readonly int JML_START_YEAR = 2012;

        [Inject]
        public ReportingDirector Director { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = "JML ITSM Interface";

            if (!this.IsPostBack)
            {
                SetCurrentMonthAndYear();

               
            }
        }

        private void SetCurrentMonthAndYear()
        {
            DateTime currentDate = DateTime.Today;

            this.monthDropDown.SelectedIndex = currentDate.Month-1;

            this.yearDropDown.SelectedIndex = currentDate.Year - JML_START_YEAR;
        }

       

        protected void exportToExcel_Click(object sender, EventArgs e)
        {
            ExportReportingData();
        }

        private void ExportReportingData()
        {
            string filename = string.Format("{0} - {1} {2}", "DirX Generated JML-ITSM Interface Tickets", monthDropDown.SelectedValue, yearDropDown.SelectedValue);

            System.Data.DataTable dt = new System.Data.DataTable("ListViewData");

            IList<JMLITSMReportData> exportData = Director.GetReportingData(monthDropDown.SelectedValue, yearDropDown.SelectedValue);

            dt.Columns.Add("Call Number");
            dt.Columns.Add("Order ID / External Call Ref.");
            dt.Columns.Add("Log DateTime");
            dt.Columns.Add("Ops Cat 1");
            dt.Columns.Add("Ops Cat 2");
            dt.Columns.Add("Ops Cat 3");
            dt.Columns.Add("CTI");
            dt.Columns.Add("Call Summary");

            foreach (var item in exportData)
            {
                dt.Rows.Add();

                dt.Rows[dt.Rows.Count - 1][0] = item.JMLReference;
                dt.Rows[dt.Rows.Count - 1][1] = item.InternalTicketId;
                dt.Rows[dt.Rows.Count - 1][2] = item.DateAdded.ToString();
                dt.Rows[dt.Rows.Count - 1][3] = item.Category;
                dt.Rows[dt.Rows.Count - 1][4] = item.Type;
                dt.Rows[dt.Rows.Count - 1][5] = item.Item;
                dt.Rows[dt.Rows.Count - 1][6] = item.OpCat;
                dt.Rows[dt.Rows.Count - 1][7] = item.Summary;

            }

            Response.AddHeader("Content-Disposition", "attachment; filename=" + filename + ".xls");
            Response.Charset = string.Empty;
            Response.ContentType = "application/vnd.xls";

            System.IO.StringWriter tw = new System.IO.StringWriter();
            System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);

            DataGrid dgGrid = new DataGrid();
            dgGrid.HeaderStyle.BackColor = System.Drawing.Color.Blue;
            dgGrid.DataSource = dt;
            dgGrid.DataBind();

            //Get the HTML for the control.
            dgGrid.RenderControl(hw);
            //Write the HTML back to the browser.
            //Response.ContentType = application/vnd.ms-excel;
          
            

            Response.Write(tw.ToString());
            Response.End();

           
        }

        protected void monthDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReloadReportingData();
        }

        private void ReloadReportingData()
        {
            JMLITSMReportListView.DataBind();
            JMLITSMReportListView.DataSourceID = "";
            JMLITSMReportListView.DataSource = Director.GetReportingData(monthDropDown.SelectedValue, yearDropDown.SelectedValue);
            JMLITSMReportListView.DataBind();
        }

        protected void yearDropDown_SelectedIndexChanged(object sender, EventArgs e)
        {
            ReloadReportingData();
        }

     

        
       
    }
}