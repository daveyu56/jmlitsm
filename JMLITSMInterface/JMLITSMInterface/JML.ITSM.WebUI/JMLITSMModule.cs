﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using JML.ITSM.BusinessLogic;
using JML.ITSM.DAL;
using JML.ITSM.Model;
using Ninject.Modules;

namespace JML.ITSM.WebUI
{
    public class JMLITSMModule : NinjectModule
    {
        public override void Load()
        {
            Bind<JMLITSMEntities>().To<JMLITSMEntities>();
            Bind<IITSMRepository>().To<ITSMRepository>();
            Bind<ITSMDirector>().To<ITSMDirector>();
            Bind<JMLITSMSetting>().To<JMLITSMSetting>();
        }   
    }
}
