﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using JML.ITSM.BusinessLogic;
using Ninject;
using Ninject.Web;

namespace JML.ITSM.WebUI.Tickets
{
    public partial class JMLITSMTickets : Ninject.Web.PageBase
    {

        [Inject]
        public TicketDirector Director { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = "JML-ITSM Interface";

            if (!IsPostBack)
            {
               
                CreateTicketsGridView.DataSource = Director.GetCreateTicketData();
                CreateTicketsGridView.DataBind();
            }
          
        }

     

        private void ViewTicketHistory(string internalTicketId)
        {
            try
            {

                Response.Redirect(string.Format("{0}{1}", "~/Tickets/JMLITSMTicketHistory.aspx?id=", internalTicketId), false);

                Context.ApplicationInstance.CompleteRequest();



            }
            catch (ThreadAbortException ex)
            {
                // do nothing
            }
            catch (Exception e)
            {

            }
            
           
          
        }

      

        protected override void Render(HtmlTextWriter writer)
        {
            // Register the dynamically created client scripts
            foreach (GridViewRow r in CreateTicketsGridView.Rows)
            {
                if (r.RowType == DataControlRowType.DataRow)
                {
                    Page.ClientScript.RegisterForEventValidation(r.UniqueID + "$ctl00");
                    Page.ClientScript.RegisterForEventValidation(r.UniqueID + "$ctl01");
                    r.Attributes["onmouseover"] = "this.style.cursor='pointer';this.style.textDecoration='underline';";
                    r.Attributes["onmouseout"] = "this.style.textDecoration='none';";
                    r.ToolTip = "Double click to view history";
                }
            }

            base.Render(writer);
        }

        protected void CreateTicketsGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
              
                LinkButton _doubleClickButton = (LinkButton)e.Row.Cells[0].Controls[0];
               
                string _jsDouble = ClientScript.GetPostBackClientHyperlink(_doubleClickButton, "");
            
                e.Row.Attributes["ondblclick"] = _jsDouble;
            }
        }

        protected void CreateTicketsGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            GridView _gridView = (GridView)sender;

            // Get the selected index and the command name
            int _selectedIndex = int.Parse(e.CommandArgument.ToString());
            string _commandName = e.CommandName;

            switch (_commandName)
            {
               
                case ("DoubleClick"):
                    string internalTicketId = (string)CreateTicketsGridView.DataKeys[_selectedIndex]["InternalTicketId"];
                    ViewTicketHistory(internalTicketId);
                    break;
            }
        }

      
    }
}