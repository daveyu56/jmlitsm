﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JML.ITSM.BusinessLogic;
using Ninject;
using Ninject.Web;

namespace JML.ITSM.WebUI.Tickets
{
    public partial class JMLITSMTicketHistory : Ninject.Web.PageBase
    {
        private string internalTicketId;

        [Inject]
        public TicketDirector Director { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = "JML-ITSM Interface";

            if (!this.IsPostBack)
            {
                internalTicketId = Request.QueryString["id"] == null ? string.Empty : Request.QueryString["id"];
                ReloadTicketData(internalTicketId);
            }
        }

        private void ReloadTicketData(string internalTicketId)
        {
            TicketDetailGridView.DataSource = Director.GetTicketDataByInternalTicketId(internalTicketId);
            TicketDetailGridView.DataBind();

           
        }

        protected override void Render(HtmlTextWriter writer)
        {
            // Register the dynamically created client scripts
            foreach (GridViewRow r in TicketDetailGridView.Rows)
            {
                if (r.RowType == DataControlRowType.DataRow)
                {
                    Page.ClientScript.RegisterForEventValidation(r.UniqueID + "$ctl00");
                    Page.ClientScript.RegisterForEventValidation(r.UniqueID + "$ctl01");
                    r.Attributes["onmouseover"] = "this.style.cursor='pointer';this.style.textDecoration='underline';";
                    r.Attributes["onmouseout"] = "this.style.textDecoration='none';";
                    r.ToolTip = "Click to view response(s), Double click to view Detail";
                }
            }

            base.Render(writer);
        }

        protected void TicketDetailGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
              
                LinkButton _singleClickButton = (LinkButton)e.Row.Cells[0].Controls[0];
             
                string _jsSingle = ClientScript.GetPostBackClientHyperlink(_singleClickButton, "");
              
                _jsSingle = _jsSingle.Insert(11, "setTimeout(\"");
                _jsSingle += "\", 300)";
              
                e.Row.Attributes["onclick"] = _jsSingle;

                LinkButton _doubleClickButton = (LinkButton)e.Row.Cells[1].Controls[0];

                string _jsDouble = ClientScript.GetPostBackClientHyperlink(_doubleClickButton, "");

                e.Row.Attributes["ondblclick"] = _jsDouble;
            }
        }


        protected void TicketDetailGridView_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            GridView _gridView = (GridView)sender;

            // Get the selected index and the command name
            int _selectedIndex = int.Parse(e.CommandArgument.ToString());
            string _commandName = e.CommandName;
            Guid masterId;

            switch (_commandName)
            {
                case ("SingleClick"):
                    masterId = (Guid)TicketDetailGridView.DataKeys[_selectedIndex]["MasterId"];
                    ViewTicketResponse(masterId);
                    break;

                case ("DoubleClick"):
                    masterId = (Guid)TicketDetailGridView.DataKeys[_selectedIndex]["MasterId"];
                    ViewTicketDetail(masterId.ToString());
                    break;
            }
        }

        private void ViewTicketResponse(Guid masterId)
        {
            TicketResponseGridView.DataSource = Director.GetTicketResponseData(masterId);
            TicketResponseGridView.DataBind(); 
        }

        private void ViewTicketDetail(string masterId)
        {
            try
            {

                Response.Redirect("~/Tickets/JMLITSMTicketDetail.aspx?id="+masterId, false);

                Context.ApplicationInstance.CompleteRequest();
               
                
               
            }
            catch (ThreadAbortException ex1)
            {
                // do nothing
            }
            catch (Exception e)
            {

            }
        }
  
    }
}