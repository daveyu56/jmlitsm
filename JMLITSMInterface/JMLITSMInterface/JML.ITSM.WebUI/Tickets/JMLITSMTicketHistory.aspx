﻿<%@ Page Title="JMLITSMTickets" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JMLITSMTicketHistory.aspx.cs" Inherits="JML.ITSM.WebUI.Tickets.JMLITSMTicketHistory" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        
    </hgroup>

    <section class="contact">

        <header>
            <h3>ITSM Request:</h3>
        </header>

        <p>

           
           <asp:GridView ID="TicketDetailGridView" runat="server" AllowPaging="True" AutoGenerateColumns="False"  OnRowCommand="TicketDetailGridView_RowCommand" OnRowDataBound="TicketDetailGridView_RowDataBound"
                 BackColor="White" BorderColor="#DEDFDE" 
                 BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical"
                 DataKeyNames="InternalTicketId, MasterId">
                
                 <Columns>
                     <asp:ButtonField Text="SingleClick" CommandName="SingleClick" Visible="false"/>
                     <asp:ButtonField Text="DoubleClick" CommandName="DoubleClick" Visible="false"/>
                     <asp:BoundField DataField="JMLReference" HeaderText="JML Reference" SortExpression="JMLReference" />
                     <asp:BoundField DataField="InternalTicketId" HeaderText="Internal Ticket Id" SortExpression="InternalTicketId" />
                     <asp:BoundField DataField="ITSMReference" HeaderText="ITSM Reference" SortExpression="ITSMReference" />
                     <asp:BoundField DataField="LoginId" HeaderText="Login Id" SortExpression="LoginId" />
                     <asp:BoundField DataField="TicketStatus" HeaderText="Ticket Status" SortExpression="TicketStatus" />
                     <asp:BoundField DataField="Summary" HeaderText="Summary" SortExpression="Summary" />
                     <asp:BoundField DataField="DateAdded" HeaderText="Date Added" SortExpression="DateAdded" />
                     <asp:BoundField DataField="OpCat1" HeaderText="Op Cat1" SortExpression="OpCat1" />
                     <asp:BoundField DataField="OpCat2" HeaderText="Op Cat2" SortExpression="OpCat2" />
                     <asp:BoundField DataField="OpCat3" HeaderText="Op Cat3" SortExpression="OpCat3" />
                     <asp:BoundField DataField="CTI" HeaderText="CTI" SortExpression="CTI" />
                     <asp:BoundField DataField="MasterId" HeaderText="MasterId" SortExpression="MasterId" Visible="false" />


                 </Columns>
                 <RowStyle BackColor="#F7F7DE" />
                 <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                  <FooterStyle BackColor="#CCCC99" />
                 <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                 <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />            
                 <AlternatingRowStyle BackColor="White" />
             </asp:GridView>
            
        </p>
      
    </section>

    <section class="contact">

         <header>
            <h3>ITSM Response:</h3>
             <p>
                <asp:GridView ID="TicketResponseGridView" runat="server" AllowPaging="True" AutoGenerateColumns="False" 
                 BackColor="White" BorderColor="#DEDFDE" 
                 BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical">
                
                 <Columns>                  
                     <asp:BoundField DataField="ResultCode" HeaderText="Result Code" SortExpression="ResultCode" />
                     <asp:BoundField DataField="ResultMessage" HeaderText="Result Message" SortExpression="ResultMessage" />
                     <asp:BoundField DataField="DateAdded" HeaderText="Date Added" SortExpression="DateAdded" />               
                 </Columns>

                 <RowStyle BackColor="#F7F7DE" />
                 <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                  <FooterStyle BackColor="#CCCC99" />
                 <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                 <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />            
                 <AlternatingRowStyle BackColor="White" />
             </asp:GridView>
             </p>
        </header>

    </section>

  
</asp:Content>