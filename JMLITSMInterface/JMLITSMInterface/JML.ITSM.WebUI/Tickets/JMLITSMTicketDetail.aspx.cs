﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JML.ITSM.BusinessLogic;
using JML.ITSM.Domain;
using Ninject;
using Ninject.Web;

namespace JML.ITSM.WebUI.Tickets
{
    public partial class JMLITSMTicketDetail : Ninject.Web.PageBase
    {
        

        [Inject]
        public TicketDirector Director { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Title = "JML-ITSM Interface";

            if (!this.IsPostBack)
            {
                string masterId = Request.QueryString["id"] == null ? string.Empty : Request.QueryString["id"];
                LoadTicketDetails(masterId);
            }
        }

        private void LoadTicketDetails(string masterId)
        {
            IList<TicketDetailRequestData> data = Director.GetTicketDetailData(masterId);

            TicketDetailsView.DataSource = data;
            TicketDetailsView.DataBind();
            
        }

      
       

       
       
      

       
  
    }
}