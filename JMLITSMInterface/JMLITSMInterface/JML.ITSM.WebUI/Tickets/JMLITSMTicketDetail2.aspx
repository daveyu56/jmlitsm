﻿<%@ Page Title="JMLITSMTickets2" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JMLITSMTicketDetail2.aspx.cs" Inherits="JML.ITSM.WebUI.Tickets.JMLITSMTicketDetail2" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        
    </hgroup>

    <section class="contact">

        <header>
            <h3>ITSM Ticket Detail:</h3>
        </header>

        <p>

           
            <asp:DetailsView ID="TicketDetailsView" runat="server" AutoGenerateRows="False"  Height="50px" Width="125px">
                <Fields>
                    <asp:BoundField DataField="MessageType" HeaderText="MessageType" SortExpression="MessageType" />
                    <asp:BoundField DataField="Priority" HeaderText="Priority" SortExpression="Priority" />
                    <asp:BoundField DataField="Severity" HeaderText="Severity" SortExpression="Severity" />
                    <asp:BoundField DataField="CallType" HeaderText="CallType" SortExpression="CallType" />
                    <asp:BoundField DataField="Source" HeaderText="Source" SortExpression="Source" />
                    <asp:BoundField DataField="ProblemText" HeaderText="ProblemText" SortExpression="ProblemText" />
                    <asp:BoundField DataField="QueueStatus" HeaderText="QueueStatus" SortExpression="QueueStatus" />
                    <asp:BoundField DataField="LastModified" HeaderText="LastModified" SortExpression="LastModified" />
                    <asp:BoundField DataField="LastModifiedBy" HeaderText="LastModifiedBy" SortExpression="LastModifiedBy" />
                    <asp:BoundField DataField="AttachmentName" HeaderText="AttachmentName" SortExpression="AttachmentName" />
                    <asp:BoundField DataField="AttachmentType" HeaderText="AttachmentType" SortExpression="AttachmentType" />
                    <asp:BoundField DataField="SentToITSMCount" HeaderText="SentToITSMCount" SortExpression="SentToITSMCount" />
                    <asp:BoundField DataField="JMLReference" HeaderText="JMLReference" SortExpression="JMLReference" />
                    <asp:BoundField DataField="InternalTicketId" HeaderText="InternalTicketId" SortExpression="InternalTicketId" />
                    <asp:BoundField DataField="ITSMReference" HeaderText="ITSMReference" SortExpression="ITSMReference" />
                    <asp:BoundField DataField="LoginId" HeaderText="LoginId" SortExpression="LoginId" />
                    <asp:BoundField DataField="TicketStatus" HeaderText="TicketStatus" SortExpression="TicketStatus" />
                    <asp:BoundField DataField="Summary" HeaderText="Summary" SortExpression="Summary" />
                    <asp:BoundField DataField="DateAdded" HeaderText="DateAdded" SortExpression="DateAdded" />
                    <asp:BoundField DataField="OpCat1" HeaderText="OpCat1" SortExpression="OpCat1" />
                    <asp:BoundField DataField="OpCat2" HeaderText="OpCat2" SortExpression="OpCat2" />
                    <asp:BoundField DataField="OpCat3" HeaderText="OpCat3" SortExpression="OpCat3" />
                    <asp:BoundField DataField="CTI" HeaderText="CTI" SortExpression="CTI" />
                    <asp:BoundField DataField="MasterId" HeaderText="MasterId" SortExpression="MasterId" />
                </Fields>
            </asp:DetailsView>
            

           
        </p>
      
    </section>

   
  
</asp:Content>
