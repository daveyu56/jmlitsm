﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using JML.ITSM.BusinessLogic;
using JML.ITSM.DAL;
using JML.ITSM.DAL.Datasources;
using JML.ITSM.Model;
using JML.ITSM.WebUI;
using Ninject;
using Ninject.Web;
using Ninject.Web.Common;


namespace JML.ITSM.WebUI
{
    public class Global : NinjectHttpApplication
    {
      
        protected override IKernel CreateKernel()
        {
            IKernel kernel = new StandardKernel(new JMLITSMModule());

            kernel.Bind<JMLITSMEntities>().To<JMLITSMEntities>();
            kernel.Bind<JMLITSMSetting>().To<JMLITSMSetting>();
            kernel.Bind<JMLITSMReportingDS>().To<JMLITSMReportingDS>();
            kernel.Bind<ReportingDirector>().To<ReportingDirector>();
            kernel.Bind<TicketDirector>().To<TicketDirector>();
            kernel.Bind<TicketDS>().To<TicketDS>();
            



            return kernel;
        }

        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterOpenAuth();
            RouteConfig.RegisterRoutes(RouteTable.Routes);

          

         


        }

        void Application_End(object sender, EventArgs e)
        {
            //  Code that runs on application shutdown

        }

        void Application_Error(object sender, EventArgs e)
        {
            // Code that runs when an unhandled error occurs

        }
    }
}
