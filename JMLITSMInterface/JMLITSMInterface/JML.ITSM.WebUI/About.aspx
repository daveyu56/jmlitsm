﻿<%@ Page Title="About" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="JML.ITSM.WebUI.About" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <hgroup class="title">
        <h1><%: Title %></h1>
     
    </hgroup>

    <article>
        <p>        
            JML-ITSM Interface Web UI.
        </p>

        <p>        
            Microsoft .NET 4.5.
        </p>

        <p>        
            &copy; Atos <%: DateTime.Now.Year %> - JML-ITSM Interface v1.0.0.0.
        </p>
    </article>

    <aside>
        <h3>JML-ITSM Web Links</h3>
        <p>        
           
        </p>
        <ul>
            <li><a  runat="server" href="~/">Home</a></li>

            <li><a  runat="server" href="~/Tickets/JMLITSMTickets">Tickets</a></li>

            <li><a  runat="server" href="~/Reporting/JMLITSMReport">Reporting</a></li>

            <li><a  runat="server" href="~/About">About</a></li>
        </ul>
    </aside>
</asp:Content>